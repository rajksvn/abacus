var current_add_bracket_id = 0

function create_tournament() {
    var create_data = {
        'tournament': $('#create_tournament_name').val(),
        'brackets': []
    }

    brackets = $('#add_bracket_form').serializeArray()
    $.each(brackets, function(i, field) {
        create_data['brackets'].push(field.value)
    })

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/create_tournament',
        data: JSON.stringify(create_data), 
        success: (function(data) {
            console.log('Create tournament success.')
        }),
        error: (function() {
            console.log('Create tournament failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You created a tournament. Head over to myAbacus to get started adding teams and players.',
                '400': 'There was a problem while creating the tournament: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to create a tournament.',
                '403': 'You do not have permission to create a tournament.',
                 '-1': 'There was an unknown problem while creating the tournament. Try again and then submit a bug report.'
            }, 'create')
        })
    })
}

function add_new_bracket_input() {
    current_add_bracket_id += 1
    bracket_id = 'add_bracket_' + current_add_bracket_id
    var to_write = ''
    to_write += '<div class="form-group floating-label-form-group col-lg-2">'
    to_write +=     '<label for="' + bracket_id + '">New bracket ' + current_add_bracket_id + '</label>'
    to_write +=     '<input type="text" name="' + bracket_id + '" id="' + bracket_id + '" class="form-control" placeholder="New bracket name">'
    to_write += '</div>'
    $('#create_add_brackets').append(to_write)
}

add_new_bracket_input()
$('#add_bracket_1').val('Preliminaries').trigger('propertychange')
