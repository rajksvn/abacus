$(function() {
    $('a.signin').on('click', function() {
        navigator.id.request({
            siteName: 'Abacus'
        })
        return false
    })
    
    $('a.signout').on('click', function() {
        navigator.id.logout()
        return false
    })

    navigator.id.watch({
        loggedInUser: CURRENT_USER,
        onlogin: function(assertion) {
            $.ajax({
                type: 'POST',
                url: URL_ROOT + '/auth/login',
                data: {assertion: assertion},
                success: function(res, status, xhr) {
                    window.location.reload()
                },
                error: function(xhr, status, err) {
                    navigator.id.logout()
                    alert('Login failure: ' + err)
                }
            })
        },    
        onlogout: function() {
            $.ajax({
                type: 'POST',
                url: URL_ROOT + '/auth/logout',
                success: function(res, status, xhr) {
                    window.location.reload()
                },
                error: function(xhr, status, err) {
                    alert('Logout failure: ' + err)
                }
            })
        }
    })
})
