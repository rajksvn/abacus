var sync_data
var player_id_list
var team_id_list
var supertournament_id = 0
var tournament_id = 0

function populate_synchronize() {
    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/populate_synchronize',
        success: (function(data) {
            console.log('Populate synchronize success.')
            
            $('#sync_button').prop('disabled', false)
            var to_write = ''
            if (Object.keys(data['brackets']).length == 0) {
                to_write += '<option value="-1" label="Nothing here yet..."></option>'
                $('#sync_button').prop('disabled', true)
            }
            else {
                to_write += '<option label="Choose a tournament to synchronize to"></option>'
                $.each(data['brackets'], function(id, bracket) {
                    var full_name = bracket['supertournament_name'] + ', ' + bracket['bracket_name'] + ' Bracket'
                    to_write += '<option'
                    to_write += ' value= ' + bracket['supertournament_id'] + '_' + id
                    to_write += ' label="' + full_name + '">'
                    to_write += full_name
                    to_write += '</option>'
                })
            }  
            $('#sync_tournaments').html(to_write)
        }),
        error: (function(){console.log('Populate synchronize failure.')})
    })
}

function synchronize() {
    var sync_tournament = $('#sync_tournaments').val().split('_')
    supertournament_id = sync_tournament[0]
    tournament_id = sync_tournament[1]

    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/synchronize/' + supertournament_id + '/' + tournament_id,
        success: (function(data) {
            console.log('Synchronize success.')
             
            $('#sync_alert').html('<div class="alert alert-success"><b>Success!</b> You\'ve synchronized to ' + data['tournament'] + '. Set up a new round to get started.</div>')

            sync_data = data
            
            tournament = data['tournament'] + ', ' + data['bracket'] + ' Bracket'
            moderator = data['moderator']
            init_tournament()
 
            rounds = data['rounds']
            var round_select = '<select id="round_select" class="form-control">'
            round_select += '<option value="-1">Choose a round</option>'
            $.each(rounds, function(round_id, round_number) {
                round_select += '<option value="' + round_id + '" label="' + round_number + '"></option>'
            })
            round_select += '</select>'
            $('#round_select_panel').html(round_select)
       
            teams = data['teams']
            var team_select = ''
            for (var i = 0; i < 2; i++) {
                var team_type = ['red', 'blu'][i]
                team_select = '<select id="' + team_type + '_select" class="form-control" onChange="populate_remote_players(\'' + team_type + '\')">'
                team_select += '<option value="-1">Choose a team</option>'
                $.each(teams, function(team_id, team) {
                    team_select += '<option value="' + team_id + '" label="' + team['name'] + '"></option>'
                })
                team_select += '</select>'
                $('#' + team_type + '_select_panel').html(team_select)
            }

            $('#red_panel').html('')
            $('#blu_panel').html('')
            $('#red_name').html('')
            $('#blu_name').html('')
            $('#statistics_panel').html('')

            $('#local_round_nav').hide()
            $('#remote_round_nav').show()

            $('#round_sync').addClass('fa fa-arrow-circle-o-right')
        }),
        error: (function(response) {
            console.log('Synchronize failure.')
            var err = $.parseJSON(response['responseText'])['message']

            if (err == 'BAD_REQUEST')
                var err_text = 'the tournament you selected does not exist.'
            else if (err == 'NOT_AUTHORIZED')
                var err_text = 'you aren\'t allowed to synchronize to the tournament you selected. Contact the tournament director to be given authorization.'
            else if (err == 'NOT_AUTHENTICATED')
                var err_text = 'you aren\'t signed in.'
            
            $('#sync_alert').html('<div class="alert alert-danger"><b>Oops!</b> We couldn\'t connect you to your tournament because ' + err_text + '</div>')

            $('#round_sync').removeClass('fa fa-arrow-circle-o-right')

            n_players = [4, 4]
            moderator = 'Abacus Moderator'
            tournament = 'Abacus Tournament'
            n_round = '1'
            team_list = ['Ashford Academy', 'School of Athens']
            name_list = [['Dolores Haze', 'Ludmilla', 'Waverly Jong', 'Offred', 'Mariane', 'Rin'], ['Le Corbusier', 'Ieoh Ming Pei', 'Eero Saarinen', 'Frank Gehry', 'Mies van der Rohe', 'Louis Sullivan']]

            tournament_id = -1

            init_vars()
            init_tournament()
            init_round()
            init_tossup(0)

            update_statistics(1)
        })
    })
}

function populate_remote_players(team_type) {
    var team_id = $('#' + team_type + '_select').val()
    var to_write = ''
    if (team_id != -1) {
        var new_players = sync_data['teams'][team_id]['players']
        $.each(new_players, function(player_id, player_name) {
            player_form_id = team_type + '_team_player_' + player_id
            to_write += '<div class="form-group floating-label-form-group col-lg-3">'
            to_write +=     '<input type="text" class="form-control" name="' + player_form_id + '" id="' + player_form_id + '" value="' + player_name +  '"> </div>'
            to_write += '</div>'
        })
    }

    $('#' + team_type + '_remote_players').html(to_write)
}

function update_sync_round() {
    $('#round_sync').removeClass('fa fa-arrow-circle-o-right')

    team_list[0] = sync_data['teams'][$('#red_select').val()]['name']
    team_list[1] = sync_data['teams'][$('#blu_select').val()]['name']
    
    team_id_list = [$('#red_select').val(), $('#blu_select').val()]

    n_round = sync_data['rounds'][$('#round_select').val()]

    var new_players = $('#remote_round_form').serializeArray()

    n_players = [0, 0]
    name_list = [[], []]
    player_id_list = [[], []]

    $.each(new_players, function(i, field) {
        var name = field.name.split('_')
        if (name[0] == 'red') {
            n_players[0]++
            name_list[0].push(field.value)
            player_id_list[0].push(name[3])
        }
        else {
            n_players[1]++
            name_list[1].push(field.value)
            player_id_list[1].push(name[3])
        }
    })

    init_vars()
    
    for (var i = 0; i < 2; i++)
        for (var j = 4; j < n_players[i]; j++)
            active[i][j] *= -1

    init_round()
    init_tossup(0)
    update_statistics()
}

