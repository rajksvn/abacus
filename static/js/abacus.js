var num_q, q_type, q, red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals, ppb_list, red_cumul, blu_cumul, n_players, points, moderator, tournament, n_round, team_list, name_list, tu_heard, active, protest_db, resolve_db, bonus_team

var edit_tu

var tournament_id

n_players = [4, 4]
moderator = 'Abacus Moderator'
tournament = 'Abacus Tournament'
bracket = 'Primary Bracket'
n_round = '1'
team_list = ['Ashford Academy', 'School of Athens']
name_list = [['Dolores Haze', 'Ludmilla', 'Waverly Jong', 'Offred', 'Mariane', 'Rin'], ['Le Corbusier', 'Ieoh Ming Pei', 'Eero Saarinen', 'Frank Gehry', 'Mies van der Rohe', 'Louis Sullivan']]

tournament_id = 0

saved = true

init_vars()
init_tournament()
init_round()
init_tossup(0)

update_statistics(1)

function init_vars() {
    num_q = 24
    q_type = 'tossup'
    q = 1
   
    red_tossups = new Array(num_q)
    red_bonus = new Array(num_q)
    red_totals = new Array(num_q)
    blu_tossups = new Array(num_q)
    blu_bonus = new Array(num_q)
    blu_totals = new Array(num_q)

    ppb_list = [0, 0]

    red_cumul = new Array(n_players[0]) //15s, 10s, -5s, totals
    blu_cumul = new Array(n_players[1])
    
    tu_heard = [new Array(n_players[0]), new Array(n_players[1])]
    active = [new Array(n_players[0]), new Array(n_players[1])]
    edit_tu = -1

    points = [[15, 10, -5], ['btn-success', 'btn-info', 'btn-danger']]
    
    protest_db = []
    resolve_db = []

    for (var i = 0; i < num_q; i++) {
        red_tossups[i] = new Array(n_players[0])
        for (var j = 0; j < n_players[0]; j++)
            red_tossups[i][j] = ''
        
        blu_tossups[i] = new Array(n_players[1])
        for (var j = 0; j < n_players[1]; j++)
            blu_tossups[i][j] = ''
        
        red_bonus[i] = ['']
        blu_bonus[i] = ['']
        red_totals[i] = ['']
        blu_totals[i] = ['']
    }
    
    var cumul_list = [red_cumul, blu_cumul]
    for (var i = 0; i < 2; i++) {
        for (var j = 0; j < n_players[i]; j++) {
            cumul_list[i][j] = new Array(4)
            for (var k = 0; k < 4; k++)
                cumul_list[i][j][k] = 0
        }
    }

    for (var i = 0; i < 2; i++) {
        for (var j = 0; j < n_players[i]; j++) {
            tu_heard[i][j] = 0
            active[i][j] = 1
        }
    }
}

function score(t, p, n) {
    saved = false
    if (q > num_q) {
        alert('Statistics sheet full. Operation aborted.')
        return;
    }

    if (t == 1 && n != 0)
        red_tossups[q - 1][p - 1] = n
    else if (n != 0) //t == 2
        blu_tossups[q - 1][p - 1] = n

    if (n == 10 || n == 15) {
        bonus_team = t
        init_bonus(bonus_team)
        q_type = 'bonus'
        update_statistics(q)
    }
    else if (n == 0) {
        q++
        init_tossup()
        update_statistics(q - 1)
    }
    else if (n == -5)
        update_statistics(q - 1)

    save_round('Recovered Round')
}

function bonus_score(t, n) {
    saved = false
    if (t == 1)
        red_bonus[q - 1][0] = n
    else
        blu_bonus[q - 1][0] = n

    q_type = 'tossup'
    q++
    init_tossup()

    update_statistics(q - 1)

    save_round('Recovered Round')
}

function edit_statistics() {
    var edit_q = Number($('#edit_q').val())
    var tossup_list = [red_tossups, blu_tossups]
    var bonus_list = [red_bonus, blu_bonus]

    if (edit_q < 1 || edit_q > num_q) {
        alert('Editing outside of range of tossups. Operation aborted.')
        return;
    }

    for (var i = 0; i < 2; i++) {
        tossup_list[i][edit_q - 1] = $.map($(['#red_tossups', '#blu_tossups'][i]).val().split(';'), function(val, j) { 
            if (val != '')
                return Number(val)
            else
                return val })
        
        edit_bonus = $(['#red_bonus', '#blu_bonus'][i]).val()

        if (edit_bonus != '')
            bonus_list[i][edit_q - 1] = [Number(edit_bonus)]
        else
            bonus_list[i][edit_q - 1] = ['']
    }
    
    update_statistics(q - 1)
}

function update_statistics(max_q) {
    var colors = ['text-danger', 'text-info']
    var stats_list = [red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals]
    var cumul_list = [red_cumul, blu_cumul]
    active_list = [['<u>',''],['</u>','']]

    calculate_statistics(max_q)

    var to_write = '<tr><th class="col-lg-1">#</th> '
    
    for (var i = 0; i < 2; i++) { //Write names
        for (var j = 0; j < n_players[i]; j++)
            to_write += '<th class="col-lg-1"><span onClick="toggle_active(' + i + ',' + j + ')" class="' + colors[i] + '">' + active_list[0][(-active[i][j] + 1) / 2] +  name_list[i][j][0] + active_list[1][(-active[i][j] + 1) / 2] + '</span></th>'
        to_write += '<th class="col-lg-1"><span class="' + colors[i] + '">B</span></th> <th class="col-lg-1"><span class="' + colors[i] + '">T</span></th> '
    }   
    to_write += '</tr>'
    
    to_write += '<tr><th class="col-lg-1"><i class="fa fa-volume-down fa-fw"></i></th> '

    for (var i = 0; i < 2; i++) { //Tossups heard
        for (var j = 0; j < n_players[i]; j++) {
            if (edit_tu == -1)
                to_write += '<td class="col-lg-1"><span onDblClick="init_edit_tu()">' + tu_heard[i][j] + '</span></td>'
            else
                to_write += '<th class="col-lg-1"><input type="text" class="form-control input-sm" id="tu_' + i + '_' + j + '" placeholder="' + tu_heard[i][j] + '" value="' + tu_heard[i][j] + '">' + '</input></th>'
        }
        to_write += '<td></td>'
            
        if (edit_tu == -1 && i == 1 || i == 0)
            to_write += '<td></td>'
        else
            to_write += '<td><button class="btn btn-success btn-xs" onClick="save_edit_tu()">Save</button></td>'
    }

    to_write += '</tr>'
    
    for (var i = 0; i < num_q; i++) { //Write individual stats by question, bonuses, and team totals
        to_write += '<tr> <td> ' + (i + 1) + '</td>' 
        for (var j = 0; j < 6; j++) {
            for (var k = 0; k < stats_list[j][i].length; k++) {

                to_write += '<td>'
                
                if ((j == 2 || j == 5) && i == q - 2)
                    to_write += '<b>'
                
                to_write += stats_list[j][i][k] 
                
                if ((j == 2 || j == 5) && i == q - 2)
                    to_write += '</b>'
                
                to_write += '</td> '

            }
        }
        to_write += '</tr>'
    }
    
    to_write += '<tr> <td>PPB</td> '
     
    for (var i = 0; i < 2; i++) { //Write PPB
        for (var j = 0; j < n_players[i]; j++)
            to_write += '<td></td>'

        to_write += '<td><span class="' + colors[i] + '">' + String(Math.round(100 * Number(ppb_list[i])) / 100) + '</span></td>'
        to_write += '<td></td>'
    }
    to_write += '</tr>'
   
    to_write += '<tr> <td></td> '
    
    for (var i = 0; i < 2; i++) { //Write names again for player totals title
        for (var j = 0; j < n_players[i]; j++)
            to_write += '<th><span class="' + colors[i] + '">' + name_list[i][j][0] + '</span></th>'
        to_write += ' <th><span class="' + colors[i] + '"></span></th> <th><span class="' + colors[i] + '"></span></th> '
    }   
    to_write += '</tr>'
    
    for (var i = 0; i < 4; i++) { //Write player totals
        to_write += '<tr> <td>' + ['15s', '10s', '-5s', 'T=='][i] + '</td> '
        for (var j = 0; j < 2; j++) {
            for (var k = 0; k < n_players[j]; k++)
                to_write += '<td>' + cumul_list[j][k][i] + '</td>'
            to_write += ' <td></td> <td></td> '
        }
        to_write += '</tr> '
    }

    $('#statistics_panel').html(to_write)
}

function calculate_statistics(max_q) {
    calculate_totals(max_q)
    calculate_cumul()
    if (q_type == 'tossup' || q_type == 'bonus')
        calculate_ppb()

    $('#red_total_header').html(red_totals[q - 2])
    $('#blu_total_header').html(blu_totals[q - 2])
}

function calculate_totals(max_q) {
    var stats_list = [red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals]

    for (var i = 0; i < 2; i++) { //Calculate team totals
        var total = 0
        for (j = 0; j < max_q; j++) {
            for (k = 0; k < n_players[i]; k++)
                total += Number(stats_list[3 * i][j][k])
            total += Number(stats_list[3 * i + 1][j][0])
            stats_list[3 * i + 2][j][0] = total
        }
    }
}

function calculate_ppb(i) {
    var stats_list = [red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals]

    for (var i = 0; i < 2; i++) { 
        var ppb = 0
        var n_bonus = 0
        var ppb_flat = []

        for (var j = 0; j < num_q; j++) //Creates a flattened bonus list
            ppb_flat[j] = stats_list[3 * i + 1][j][0]

        for (var j = 0; j <= 30; j += 10) //Calculates total number of bonuses heard (also number of tossups heard, interesting)
            n_bonus += number_of(Number(j), ppb_flat)

        try {
            ppb = stats_list[3 * i + 1].reduce(function(a, b) { return Number(a) + Number(b) }) / n_bonus
        }
        catch (err) {}

        ppb_list[i] = ppb
    }
}

function calculate_cumul() {
    var stats_list = [red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals]
    var cumul_list = [red_cumul, blu_cumul]

    for (var i = 0; i < 2; i++) { //Calculate player totals
        for (var j = 0; j < n_players[i]; j++) { // j represents player
            var player_scores = new Array(num_q)
            for (var k = 0; k < num_q; k++) //Creates a list of scores for each player
                player_scores[k] = stats_list[3 * i][k][j]
            
            cumul_list[i][j][3] = 0 //Total
            for (var k = 0; k < 3; k++) { // k represents pt value in list [15, 10, -5], 3 = total
                var to_score = [15, 10, -5][k]
                cumul_list[i][j][k] = number_of(to_score, player_scores)
                cumul_list[i][j][3] += cumul_list[i][j][k] * to_score
            }
        }
    }
}

function number_of(val, list) {
    var c = 0
    for (var i = 0; i < list.length; i++)
        if (list[i] === val)
            c++
    
    return c
}

function init_tournament() {
    $('#moderator').html(moderator)
    if (tournament_id == 0)
        $('#tournament').html(tournament)
    else
        $('#tournament').html('<a href="' + SCRIPT_ROOT + '/load/' + supertournament_id + '" target="_blank" style="color:white">' + tournament + '</a>' + '<i class="fa fa-external-link-square fa-fw"></i>')
}

function init_round() {
    $('#red_name').html(team_list[0])
    $('#blu_name').html(team_list[1])
    $('#n_round').html(n_round)
    $('#red_total_header').html('')
    $('#blu_total_header').html('')
}

function init_tossup(new_q) {
    var panel_list = ['#red_panel', '#blu_panel']
    var btn_list = ['btn-danger', 'btn-primary']
    var btn_inactive = ['btn-default', 'btn-default']
    var header_script = ['<button type="button" class="btn btn-full btn-default" onClick="score(1,1,0)">Dead Tossup</button>', '<a data-toggle="modal" data-target="#protest" onClick="init_protest(\'protest_team\')" role="button" class="btn btn-full btn-danger">Record Protest</a>']

    for (var i = 0; i < 2; i++) {
        var to_write = '<div class="col-lg-10 col-centered">'
            
        to_write += '<div class="btn-group btn-group-sm btn-group-sm-spread">' + header_script[i] + '</div> <br>'
        for (var j = 0; j < n_players[i]; j++) {
            if (active[i][j] == 1) {         
                to_write += '<div class="btn-group btn-group-sm btn-group-sm-spread">' + '<button type="button" class="btn btn-danger" onClick="toggle_active(' + i + ',' + j + ')"><i class="fa fa-refresh fa-fw"></i></button>' + '<button class="btn ' + btn_list[i] + ' btn-fixed-width">' + name_list[i][j]
                for (var k = 0; k < points[0].length; k++)
                    to_write += '<button type="button" class="btn ' + points[1][k] + '" onClick="score(' + (i + 1) + ',' + (j + 1) + ',' + points[0][k] + ')">' + points[0][k] + '</button>'
                to_write += '</div> <br>'
            }
            else {
                to_write += '<div class="btn-group btn-group-sm btn-group-sm-spread">' + '<button type="button" class="btn btn-danger" onClick="toggle_active(' + i + ',' + j + ')"><i class="fa fa-refresh fa-fw"></i></button>' + '<button class="btn ' + btn_inactive[i] + ' btn-fixed-width">' + name_list[i][j]
                for (var k = 0; k < points[0].length; k++)
                    to_write += '<button type="button" class="btn ' + btn_inactive[i] + '">' + points[0][k] + '</button>'
                to_write += '</div> <br>'
            }
                
        }
        $(panel_list[i]).html(to_write)
    }
     
    if (new_q != 0) {
        for (var i = 0; i < 2; i++)
            for (var j = 0; j < n_players[i]; j++)
                if (active[i][j] == 1)
                    tu_heard[i][j] += 1
    }

    $('#qnum').html(q)
    $('#qtype').html('Tossup')
    
    $('#bonus').css("z-index", 1)
    $('#tossup').css("z-index", 2)

    $('#dead').html('<button type="button" class="btn btn-default btn-xs btn-full" onClick="score(1, 1, 0)">Dead Tossup</button>')
} 

function init_bonus(t) {
    if (t == 1) {
        bonus_t_colors = ['label-danger', 'label-default']
        bonus_p_colors = [['btn-success', 'btn-info', 'btn-warning', 'btn-danger'], ['btn-default', 'btn-default', 'btn-default', 'btn-default']]
        bonus_script = ['', '"" id=']
    }
    else {
        bonus_t_colors = ['label-default', 'label-primary']
        bonus_p_colors = [['btn-default', 'btn-default', 'btn-default', 'btn-default'], ['btn-success', 'btn-info', 'btn-warning', 'btn-danger']]
        bonus_script = ['"" id=', '']
    }
   
    var bonus_points = [30, 20, 10, 0]
    var panel_list = ['#red_panel', '#blu_panel']
    
    for (var i = 0; i < 2; i++) {
        to_write = ''
        to_write += '<div class="col-lg-12 text-center"> <span class="label ' + bonus_t_colors[i] + '">' + team_list[i] + '</span> <p></p> <div class="btn-group"> '
        for (var j = 0; j < bonus_points.length; j++)
            to_write += ' <button type="button" class="btn ' + bonus_p_colors[i][j] + ' bonus-btn" onClick=' + bonus_script[i] + '"bonus_score(' + t + ', ' + bonus_points[j] + ')">' + bonus_points[j] + '</button> '
        to_write += '</div>'

        $(panel_list[i]).html(to_write)
    }

    $('#qtype').html('Bonus')
        
    $('#tossup').css("z-index", 1)
    $('#bonus').css("z-index", 2)
    
    $('#dead').html('')
}

function update_round() {
    team_list[0] = $('#red').val()
    team_list[1] = $('#blu').val()
    n_round = $('#n_round_in').val()

    n_players[0] = Math.min($('#n_red').val(), 8)
    n_players[1] = Math.min($('#n_blu').val(), 8)
    
    for (var i = 0; i < 2; i++) {
        name_list[i] = new Array(n_players[i])
        for (var j = 0; j < n_players[i]; j++)
            name_list[i][j] = $(['#red_', '#blu_'][i] + (j + 1)).val()    
    }

    init_vars()
    
    for (var i = 0; i < 2; i++)
        for (var j = 4; j < n_players[i]; j++)
            active[i][j] *= -1

    init_round()
    init_tossup(0)
    update_statistics()
}

function update_tournament() {
    tournament = $('#tournament_name').val()
    moderator = $('#moderator_name').val()
    
    init_tournament()
}

function undo() { 
    if (q == 1 && q_type == 'tossup' && red_tossups[q - 1].indexOf(-5) == -1 && blu_tossups[q - 1].indexOf(-5) == -1) {
        alert('No questions to undo. Operation aborted.')
        return;
    }

    stats_list = [red_tossups, red_bonus, red_totals, blu_tossups, blu_bonus, blu_totals]
    if (q_type == 'tossup' && red_tossups[q - 1].indexOf(-5) == -1 && blu_tossups[q - 1].indexOf(-5) == -1) { //_bonus was previously written, and thus next question has started and q is inaccurate
        q = q - 1
        for (var i = 0; i < 2; i++)
            for (var j = 0; j < n_players[i]; j++)
                if (active[i][j] == 1)
                    tu_heard[i][j] -= 1
    }
    else
        q_type = 'tossup'

    for (var i = 0; i < 2; i++)
        for (var j = 0; j < n_players[i]; j++)
            if (active[i][j] == 1)
                tu_heard[i][j] -= 1

    for (var i = 0; i < stats_list.length; i++)
        for (var j = 0 ; j < stats_list[i][q - 1].length; j++)
            stats_list[i][q - 1][j] = ''

    init_tossup()
    update_statistics(q - 1)
}

function clear_new_round() {
    $('#new_round_form')[0].reset()
    $('#red_players').html('')
    $('#blu_players').html('')
}

function toggle_active(t, p) {
    active[t][p] *= -1
    update_statistics(q - 1)
    if (q_type == 'tossup') {
        init_tossup(0)
    }
}

function init_edit_tu() {
    edit_tu *= -1
    update_statistics(q - 1)
}

function save_edit_tu() {
   for (var i = 0; i < 2; i++)
       for (var j = 0; j < n_players[i]; j++)
           tu_heard[i][j] = Number($('#tu_' + i + '_' + j).val())
    edit_tu *= -1
    update_statistics(q - 1)
}

function reset_round_statistics() {
    update_round()
}

function save_round(name) {
    var var_list = ['num_q', 'q_type', 'q', 'red_tossups', 'red_bonus', 'red_totals', 'blu_tossups', 'blu_bonus', 'blu_totals', 'ppb_list', 'red_cumul', 'blu_cumul', 'n_players', 'points', 'moderator', 'tournament', 'n_round', 'team_list', 'name_list', 'tu_heard', 'active', 'protest_db', 'resolve_db', 'bonus_team']

    var var_dict = {}

    for (var i = 0; i < var_list.length; i++)
        var_dict[var_list[i]] = window[var_list[i]]
 
    if (name)
        cookie_name = name
    else
        cookie_name = $('#save_name').val()
    
    $.cookie.json = true
    $.cookie.raw = true
    $.removeCookie(cookie_name)
    $.cookie(cookie_name, var_dict)
}

function load_cookies() { 
    var cookie_list = $.cookie()

    var to_write = '<select multiple class="form-control" id="load_name">'

    for (var key in cookie_list)
        to_write += '<option>' + key + '</option>'

    to_write += '</select>'

    $('#cookies').html(to_write)
}

function load_round() {
    loaded = JSON.parse($.cookie()[$('#load_name').val()])
    
    var var_list = ['num_q', 'q_type', 'q', 'red_tossups', 'red_bonus', 'red_totals', 'blu_tossups', 'blu_bonus', 'blu_totals', 'ppb_list', 'red_cumul', 'blu_cumul', 'n_players', 'points', 'moderator', 'tournament', 'n_round', 'team_list', 'name_list', 'tu_heard', 'active', 'protest_db', 'resolve_db', 'bonus_team']
     
    for (var i = 0; i < var_list.length; i++)
        window[var_list[i]] = loaded[var_list[i]]
    
    if (protest_db.length != 0)
        $('#n_unresolv').html(protest_db.length)
    else
        $('#n_unresolv').html('')

    init_tournament()
    init_round()
    
    if (q_type == 'tossup')
        init_tossup(0)
    if (q_type == 'bonus')
        init_bonus(bonus_team)

    update_statistics()
}

function delete_round() {
    $.cookie.raw = true
    $.removeCookie($('#load_name').val()[0])
    load_cookies()
}

function init_protest(id) {
    $('#record_protest_form')[0].reset()

    $('#protest_q').val(q)
    $('#' + id)
        .empty()
        .append(
            '<option>' + team_list[0] + '</option>', 
            '<option>' + team_list[1] + '</option>'    
        )
}

function protest() {
    protest_db.push({
        'protest_q': $('#protest_q').val(),
        'protest_pts': $('#protest_pts').val(),
        'protest_team': $('#protest_team').val(),
        'protest_info': $('#protest_info').val(),
        'protest_action': $('#protest_action').val()
    })
    $('#n_unresolv').html(protest_db.length)
}

function populate_protest(id, type) {
    $('#open_protest_form')[0].reset()
    $('#closed_protest_form')[0].reset()

    var to_write = ''
    if (type == 'open')
        protest_list = protest_db
    else
        protest_list = resolve_db

    for (var i = 0; i < protest_list.length; i++)
        to_write += '<option value="' + i + '" label="' + protest_list[i]['protest_info'].substring(0,60) + '...' + '"></option>,'

    $('#' + id)
        .empty()
        .append(to_write)
}

function resolve_protest() {
    protest_db[Number($('#protest_id').val())]['protest_resolve'] = $('#protest_resolve').val()
    
    resolve_db.push((protest_db[Number($('#protest_id').val())]))

    protest_db.splice(Number($('#protest_id').val()), 1)
    
    if (protest_db.length != 0)
        $('#n_unresolv').html(protest_db.length)
    else
        $('#n_unresolv').html('')
}

function delete_protest() {
    protest_db.splice(Number($('#protest_id').val()), 1)
    populate_protest('protest_id', 'open')

    if (protest_db.length != 0)
        $('#n_unresolv').html(protest_db.length)
    else
        $('#n_unresolv').html('')
}

$('#edit_q').keyup(function() {
    if (strip($('#edit_q').val()) == '') {
        $('#red_tossups').val('').trigger('propertychange')
        $('#blu_tossups').val('').trigger('propertychange')
        $('#red_bonus').val('').trigger('propertychange')
        $('#blu_bonus').val('').trigger('propertychange')
        return;
    }

    var edit_q = $('#edit_q').val() - 1
    var tossup_list = [red_tossups, blu_tossups]
    var bonus_list = [red_bonus, blu_bonus]

    for (var i = 0; i < 2; i++) {
        var to_write = ''
        for (var j = 0; j < n_players[i]; j++)
             to_write += tossup_list[i][edit_q][j] + ';'
        to_write = to_write.slice(0, -1)
        
        $(['#red_tossups','#blu_tossups'][i]).val(to_write).trigger('propertychange')
        $(['#red_bonus','#blu_bonus'][i]).val(bonus_list[i][edit_q][0]).trigger('propertychange')
    }
})

function populate_round_players(mode) {
    var to_write = '<div class="row">'
    for (var i = 0; i < Math.min($('#n_' + mode + '').val(), 8); i++) {
        to_write += '<div class="form-group col-lg-3 floating-label-form-group">'
        to_write += '<label for="' + mode + '_' + (i + 1) + '">Player ' + (i + 1) + '</label>'
        to_write += '<input type="text" class="form-control" id="' + mode + '_' + (i + 1) + '" placeholder="Player ' + (i + 1) + '">' 
        to_write += '</div>'
        if ((i + 1) % 4 == 0) {
            to_write += '</div>'
            to_write += '<div class="row">'
        }
    }
    to_write += '</div>'
        
    $('#' + mode + '_players').html(to_write)
}

$('#protest_id').change(function() {
    init_protest('static_protest_team')
    protest_object = protest_db[Number($('#protest_id').val())]
    $('#static_protest_q').val(protest_object['protest_q'])
    $('#static_protest_pts').val(protest_object['protest_pts'])
    $('#static_protest_team').val(protest_object['protest_team'])
    $('#static_protest_info').val(protest_object['protest_info'])
    $('#static_protest_action').val(protest_object['protest_action'])
})

$('#c_protest_id').change(function() {
    init_protest('c_static_protest_team')
    protest_object = resolve_db[Number($('#c_protest_id').val())]
    $('#c_static_protest_q').val(protest_object['protest_q'])
    $('#c_static_protest_pts').val(protest_object['protest_pts'])
    $('#c_static_protest_team').val(protest_object['protest_team'])
    $('#c_static_protest_info').val(protest_object['protest_info'])
    $('#c_static_protest_action').val(protest_object['protest_action'])
    $('#c_static_protest_resolve').val(protest_object['protest_resolve'])
})

function clear_upload() {
    $('#upload_data').html('')
    $('#upload_alert').html('')
    $('#upload_info').html('')
}

function populate_upload(div_id) {
    if (tournament_id == -1) {
        $('#upload_alert').html('<div class="alert alert-danger">You must synchronize to a tournament in the Abacus Database before uploading.</div>')
        $('#upload_db_btn').attr('onclick', '')
        $('#upload_db_btn').removeClass('btn-primary')
        $('#upload_db_btn').addClass('btn-default')
        return
    }

    if (q <= 1) {
        $('#upload_alert').html('<div class="alert alert-danger">You need to start a round and fill in statistics for at least one question before uploading.</div>')
        $('#upload_db_btn').attr('onclick', '')
        $('#upload_db_btn').removeClass('btn-primary')
        $('#upload_db_btn').addClass('btn-default')
        return
    }
    
    if (supertournament_id == 0) {
        $('#upload_alert').html('<div class="alert alert-danger">You need to synchronize to a tournament before uploading statistics.</div>')
        $('#upload_db_btn').attr('onclick', '')
        $('#upload_db_btn').removeClass('btn-primary')
        $('#upload_db_btn').addClass('btn-default')
        return
    }
    
    $('#upload_alert').html('')
    $('#upload_db_btn').attr('onclick', 'upload_database()')
    $('#upload_db_btn').removeClass('btn-default')
    $('#upload_db_btn').addClass('btn-primary')

    var colors = ['text-danger', 'text-info']
    var cumul_list = [red_cumul, blu_cumul]

    var name_dict = {}
    var tu_dict = {}

    for (var i = 0; i < 2; i++) {
        for (var j = 0; j < n_players[i]; j++) {
            name_dict[['red', 'blu'][i] + '_' + String(j + 1)] = name_list[i][j]
            tu_dict[['red', 'blu'][i] + '_' + String(j + 1) + '_tu'] = tu_heard[i][j]
        }
    }

    var cumul_list = [red_cumul, blu_cumul]
    var cumul_points = ['15', '10', 'n5', 'T']
    var cumul_dicts = [{}, {}]
    for (var i = 0; i < 2; i++)
        for (var j = 0; j < cumul_list[i].length; j++)
            for (var k = 0; k < cumul_list[i][j].length; k++)
                    cumul_dicts[i][['red', 'blu'][i] + '_' + String(j + 1) + '_' + cumul_points[k]] = cumul_list[i][j][k]
    
    totals_list = [red_totals[q - 2][0], blu_totals[q - 2][0]]

    var overview_dict = {
        'red_name': team_list[0],
        'blu_name': team_list[1],
        'red_total': String(totals_list[0]),
        'blu_total': String(totals_list[1]),
        'red_ppb': ppb_list[0],
        'blu_ppb': ppb_list[1],
        'n_red': n_players[0],
        'n_blu': n_players[1],
        'n_round': n_round,
        'moderator': moderator,
        'tournament': tournament
    }

    to_write = '<b><p style="text-align:center;">'
    to_write += tournament + '<br>'
    to_write += 'Round ' + n_round + '<br>'
    to_write += 'Moderator ' + moderator
    to_write += '</p></b>'

    for (var i = 0; i < 2; i++) {
        to_write += '<div class="col-lg-6"><p style="text-align:' + ['right', 'left'][i] + ';">'
        to_write += '<b><span class="' + colors[i] + '">' + team_list[i] + ': ' + totals_list[i] + ' @ ' + roundStatistic(ppb_list[i]) + '</span></b> <br>'
        for (var j = 0; j < n_players[i]; j++) {
            to_write += '<span class="' + colors[i] + '">' + name_list[i][j] + '</span> <br>'
        }
        to_write += '</p></div>'
    }

    to_write += '<table class="table table-striped table-hover table-condensed table-responsive">'

    to_write += '<tr> <td></td> '
    for (var i = 0; i < 2; i++) { //Write names again for player totals title
        for (var j = 0; j < n_players[i]; j++)
            to_write += '<th><span class="' + colors[i] + '">' + name_list[i][j][0] + '</span></th>'
        
        if (i == 0)
            to_write += ' <th></th> '
    }   
    to_write += '</tr>'

    to_write += '<tr> <td><i class="fa fa-volume-down fa-fw"></i></td> '
    for (var i = 0; i < 2; i++) {
        for (var j = 0; j < n_players[i]; j++)
            to_write += '<td>' + tu_heard[i][j] + '</td>'
        if (i == 0)
            to_write += ' <td></td> '
    }   
    to_write += '</tr>'

    for (var i = 0; i < 4; i++) { //Write player totals
        to_write += '<tr> <td>' + ['15s', '10s', '-5s', 'T=='][i] + '</td> '
        for (var j = 0; j < 2; j++) {
            for (var k = 0; k < n_players[j]; k++)
                to_write += '<td>' + cumul_list[j][k][i] + '</td>'
            if (j == 0)
                to_write += ' <td></td> '
        }
        to_write += '</tr> '
    }

    to_write += '</table>'

    $('#' + div_id).html(to_write)

    $('#upload_info').html('<div class="alert alert-info"><b>Look good?</b> Go ahead and hit Upload. If not, close and fix any mistakes.</div>')
}

function tabulate_statistics() {
    var cumul_list = [red_cumul, blu_cumul]
    var totals_list = [red_totals, blu_totals]
    var teams = []

    for (var i = 0; i < 2; i++) {
        var players = []
        var total_player_total = 0
        for (var j = 0; j < n_players[i]; j++) {
            var player_id = player_id_list[i][j] 
            
            var player_stats = {}
            for (var k = 0; k < cumul_list[i][j].length; k++) {
                player_stats[['15', '10', 'n5', 'T'][k]] = cumul_list[i][j][k]
                if (k < 3)
                    total_player_total += [15, 10, -5][k] * cumul_list[i][j][k] 
            }
            players.push($.extend({
                'id':Number(player_id), 
                'name':name_list[i][j], 
                'heard':tu_heard[i][j]
            }, player_stats))
        }

        teams.push({
            'id':Number(team_id_list[i]),
            'name':team_list[i], 
            'players':players,
            'total':totals_list[i][q - 2][0],
            'bonus_total':totals_list[i][q - 2][0] - total_player_total,
            'ppb':ppb_list[i],
            'heard':(q - 1)
        })
    }

    return {
        'tournament_name': tournament,
        'moderator_name': moderator,
        'n_round': n_round,
        'teams':teams
    }
}

function upload_database() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/upload_database/' + supertournament_id + '/' + tournament_id,
        data: JSON.stringify(tabulate_statistics()),
        success: (function() {
            console.log('Database upload success.')
            clear_upload()
            saved = true
            $('#upload_alert').html('<div class="alert alert-success"><b>Success!</b> Your data has been uploaded to ' + tournament + '.</div>')
            $('#upload_info').html('<div class="alert alert-info">You can verify that the upload succeeded <a href="' + SCRIPT_ROOT + '/load/' + supertournament_id + '" target="_blank">here</a>.</div>')
        }),
        error: (function() {
            console.log('Database upload failure.')
            clear_upload()
            $('#upload_alert').html('<div class="alert alert-danger"><b>Oops!</b> We couldn\'t upload your data. Try again.<br>If the problem persists, save a local copy of the round and e-mail the statistics to your director.</div>')
        })
    })

    clear_upload()
}

$(function() {
    $('#upload-file-btn').click(function() {
        var form_data = new FormData($('#upload-file')[0])
        $.ajax({
            type: 'POST',
            url: SCRIPT_ROOT + '/upload_packet',
            data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: false,
            success: function(data) {
                console.log('Upload success.')
                var packet_frame = '<object width="100%" height="100%" type="application/pdf" data="' + data['filename'] + '"></object>'
                $('#tossup').html(packet_frame)                
                $('#bonus').html(packet_frame)                
                init_tossup(0)
            },
            error: function(data) {
                console.log('Upload failure.')
                alert('Your file failed to upload. Please try again.')
            }
        })
    })
})

function random_fill() {
    var team, player, pts_code, new_pts_code, bonus_pt_code
    for (var i = 0; i < 20; i++) {
        team = randomInt(1, 2)
        player = randomInt(1, n_players[team - 1])
        pts_code = randomInt(1, 10)
        
        if (pts_code <= 3)
            score(team, player, 15)
        else if (pts_code <= 7)
            score(team, player, 10)
        else if (pts_code <= 9) {
            score(team, player, -5)

            team = -team + 3
            player = randomInt(1, n_players[team - 1])
            new_pts_code = randomInt(1, 2)
            if (new_pts_code == 1)
                score(team, player, 15)
            else if (new_pts_code == 2)
                score(team, player, 10)
        }
        else if (pts_code == 10)
            score(1, 1, 0)
        
        if (1 <= pts_code && pts_code <= 9) {
            bonus_pts_code = randomInt(1,4)
            bonus_score(team, (bonus_pts_code - 1) * 10)
        }
    }
}

function randomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function generate_alert(type, message, prefix) {
    var to_write = '' +
    '<div class="alert alert-' + type + ' alert-dismissable">' +
        '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' +
        '<strong>' + prefix + '</strong> ' + 
        message +
    '</div>'
    return to_write
}
