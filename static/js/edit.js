var modes = ['tournament', 'bracket', 'credential', 'team', 'game']

var current_add_id = 0
var add_ids = {}

var current_add_division_id = 0

var edit_data

var add_game_bracket_id = 0

$(document).ready(function() {
    path = window.location.pathname.split('/')
    if (path[1] == 'edit')
        load_edit_data()
})

function get_supertournament_id() {
    return window.location.pathname.split('/')[2]
}

function get_tournament_id() {
    return window.location.pathname.split('/')[3]
}

$(document).on('change keypress paste textInput input', '.input-track-change', function() {
    $(this).addClass('input-edited')
})

function load_edit_data() {
    $('#edit_alert').html('')
    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/get_edit_data/' + get_supertournament_id(),
        success: (function(data) {
            console.log('Load edit tournament success.')

            $('.input-track-change').removeClass('input-edited')
           
            hide_add_team()
            hide_add_game()

            $('#supertournament_name').html(
                data['supertournament']
            )

            edit_data = data

            load_overview(data)
            
            load_credentials('director', 'dir', data['directors'])
            load_credentials('moderator', 'mod', data['moderators'])
            
            load_brackets(data['brackets'])
            load_teams(data['teams'])
        }),
        error: (function() {
            console.log('Load edit tournament failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You loaded edit data.',
                '401': 'You need to be logged to load edit data.',
                '403': 'You do not have permission to load edit data.',
                 '-1': 'There was an unknown problem while loading edit data. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function parse_response(response, response_messages, alert_destination) {
    var alert_location = '#' + alert_destination + '_alert'

    var timeout = 500

    $(alert_location).fadeTo(0, 500).slideDown(500, function(){
        $(this).show()
    });
    var done = false
        
    $.each(response_messages, function(code, message) {
        if (response.status == code) {
            if (response.status == 200) {
                alert_type = 'success'
                alert_prefix = 'Success!'
            }
            else {
                alert_type = 'danger'
                alert_prefix = 'Oops!'
            }
                
            $(alert_location).append(generate_alert(
                alert_type,
                message,
                alert_prefix
            ))

            done = true
        }
    })
    
    if (!done) {
        $(alert_location).append(generate_alert(
            'danger',
            response_messages['-1'],
            'Oops!'
        ))
    }
    
    window.setTimeout(function() {
        $(alert_location).fadeTo(500, 0).slideUp(500, function(){
            $(this).html('')
            $(this).hide()
        });
    }, 2000);
}

function regenerate_statistics() {
    $('#update_spinner').fadeIn()
    $.ajax({
        url: '/update_statistics/' + get_supertournament_id(),
        success: (function(response) {
            console.log('Update statistics success.')
        }),
        error: (function(response) {
            console.log('Update statistics failure.')
        }),
        complete: (function(response) {
        $('#update_spinner').fadeOut()
            parse_response(response, {
                '200': 'You updated statistics.',
                 '-1': 'There was an unknown problem while updating statistics. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

/* --------------------------
     Tournament Settings
---------------------------*/
function load_overview(data) {
    $('#edit_supertournament_name').val(data['supertournament']).trigger('propertychange')
}

function edit_settings() {
    var settings_data = {
        'tournament_name':$('#edit_supertournament_name').val(),
    }
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/edit_settings/' + get_supertournament_id(),
        data: JSON.stringify(settings_data),
        success: (function(data) {
            console.log('Edit tournament settings success.')
            load_edit_data()
        }),
        error: (function(response) {
            console.log('Edit tournament settings failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You edited your tournament settings.',
                '400': 'There was a problem while changing the tournament settings: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to change tournament settings.',
                '403': 'You do not have permission to change the tournament settings.',
                 '-1': 'There was an unknown problem while changing the tournament settings. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function delete_tournament() {
    if (!confirm('Are you sure you want to delete this tournament? All data will be permanently lost.'))
        return

    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/delete_tournament/' + get_supertournament_id(),
        success: (function(data) {
            console.log('Delete tournament success.')
            window.location.href = SCRIPT_ROOT + '/account' 
        }),
        error: (function(response) { 
            console.log('Delete tournament failure.')
            //This block is in error instead of complete because success will always go back to myAbacus 
            parse_response(response, {
                '401': 'You need to be logged in to delete this tournament.',
                '403': 'You do not have permission to delete this tournament.',
                 '-1': 'There was an unknown problem while deleting this tournament. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

/* ---------------------------
   Directors and Moderators
----------------------------*/
function load_credentials(mode, abbr, users) {
    var to_write = ''

    $.each(users, function(id, user) {
        to_write += '<p>'
        to_write += user['email']
        to_write += '<small> <a href="#" onClick="delete_credentials(\'' + mode + '\',\'' + abbr + '\',' + user['id'] + ')">(delete)</a> </small>'
        to_write += '</p>'
    })

    $('#' + mode + '_list').html(to_write)
}

function add_credentials(mode) {
    emails = $('#add_credential_list').val()

    if ($.trim(emails) == '') {
        reset_credentials()
        return;
    }

    var to_add = []
    emails = emails.split(',')

    for (var i = 0; i < emails.length; i++)
        to_add.push($.trim(emails[i]))

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/add_credentials/' + get_supertournament_id(),
        data: JSON.stringify({
            'emails': to_add, 
            'type': mode
        }),
        success: (function(response) {
            console.log('Add ' + mode + 's success.')
            reset_credentials()    
        }),
        error: (function(response) {
            console.log('Add ' + mode + 's failure.')
        }),
        complete: (function(response) {
            load_edit_data()
            parse_response(response, {
                '200': 'You added ' + mode + '(s).',
                '400': 'We couldn\'t add <strong>' + (response && response.responseJSON && response.responseJSON.non_users && response.responseJSON.non_users.join(', ') || '') + '</strong> as ' + mode + '(s) because they do not have Abacus accounts. Creating an account is as easy as logging in at the top right.',
                '401': 'You need to be logged in to add ' + mode + '(s).',
                '403': 'You do not have permission to add ' + mode + '(s).',
                 '-1': 'There was an unknown problem while adding ' + mode + '(s). Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function delete_credentials(mode, abbr, id) {
    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/delete_credentials/' + get_supertournament_id() + '/args',
        data: {'user_id':id, 'type':mode},
        success: (function(data) {
            console.log('Delete ' + mode + 's success.')
            reset_credentials()
            $('#add_cred_name').val('')
            $('#add_cred_name').hide()
        }),
        error: (function() {
            console.log('Delete ' + mode + 's failure.')
        }),
        complete: (function(response) {
            load_edit_data()
            parse_response(response, {
                '200': 'You deleted ' + mode + '(s).',
                '400': 'You can\'t delete the last director for this tournament.',
                '401': 'You need to be logged in to delete ' + mode + '(s)',
                '403': 'You do not have permission to delete ' + mode + '(s)',
                 '-1': 'There was an unknown problem while deleting ' + mode + '(s). Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function reset_credentials() {
    $('#add_credential_list').val('').trigger('propertychange')
}

/* --------------------------
          All Teams         
---------------------------*/
function load_teams(teams) {
    division_select = ''
    var to_write = ''
    $.each(teams, function(id, team) {
        team_prefix = 'division_team_' + team['id']
        add_ids[team['id']] = 0
        title_length = Math.min(team['players'].length * 2, 12)

        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12">'
        to_write +=         '<div class="row">'
        to_write +=             '<div class="form-group floating-label-form-group col-sm-' + title_length + '">'
        to_write +=                 '<label for="team_' + team['id'] +'">Team</label>'
        to_write +=                 '<input type="text" name="team_' + team['id'] + '" id="team_' + team['id'] + '" class="form-control input-track-change" value="' + team['name'] + '" style="font-weight:bold;" placeholder="Team name">'
        to_write +=             '</div>'
        to_write +=         '</div>'
        to_write +=         '<div class="row" id="team_' + team['id'] + '_players">'
        $.each(team['players'], function(id, player) {
            to_write +=     '<div class="form-group floating-label-form-group col-sm-2">'
            to_write +=         '<label for="player_' + player['id'] +'">Player</label>'
            to_write +=         '<input type="text" name="player_' + player['id'] + '" id="player_' + player['id'] + '" class="form-control input-track-change" value="' + player['name'] + '" placeholder="Player name">'
            to_write +=     '</div>'
        })
        to_write +=         '</div>'
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<br>'
        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12" style="font-size:75%;">'
        to_write +=         '<div class="row">'
        to_write +=             generate_bracket_divisions(edit_data['brackets'], team_prefix)
        to_write +=             '<small>Controls</small> <br>'
        to_write +=             '<small><a href="javascript:void(0);" onClick="edit_teams_add_player(' + team['id'] + ')">Add player</a></small> <br>'
        to_write +=             '<small><a href="javascript:void(0);" onClick="delete_team(' + team['id'] + ')">Delete team</a></small> <br>'
        to_write +=         '</div>'
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<hr>'
    })
    $('#team_list').html(to_write)
        
    $.each(teams, function(id, team) {
        $('#team_' + team['id']).trigger('propertychange')
        $.each(team['players'], function(id, player) {
            $('#player_' + player['id']).trigger('propertychange')
        })
        $.each(team['divisions'], function(bracket_id, division_id) {
            $('#division_team_' + team['id'] + '_bracket_' + bracket_id).val(division_id)
        })
    })
}

function show_add_team() {
    hide_add_team()
    hide_import_teams()

    for (var i = 0; i < 4; i++)
        add_new_player_input() 
    
    var to_write = ''
    
    $.each(edit_data['brackets'], function(id, bracket) {
        select_name = 'new_team_division_select_bracket_' + id
        to_write += bracket['name'] + ' Bracket Division Selection'
        to_write += '<select name="' + select_name + '" id="' + select_name + '" class="form-control">'
        to_write +=     generate_team_divisions(bracket['divisions'])
        to_write += '</select>'
        to_write += '<br>'
    })
    $('#new_team_division').html(to_write)
    
    $('#add_team_subpanel').show()
}

function generate_bracket_divisions(brackets, team_prefix) {
    var division_select = ''
    $.each(edit_data['brackets'], function(id, bracket) {
        var division_id = team_prefix + '_bracket_' + id
        division_select += '<div class="col-sm-2">'
        division_select +=      '<small>' + bracket['name'] + ' bracket division</small>'
        division_select +=      '<select name="' + division_id + '" id="' + division_id + '" class="form-control input-track-change">'
        division_select +=          generate_team_divisions(bracket['divisions'])
        division_select +=      '</select>'
        division_select += '</div>'
    })
    return division_select
}

function generate_team_divisions(divisions) {
    var to_write = ''
    $.each(divisions, function(id, division_name) {
        division_id = id
        to_write += '<option name="' + division_id + '" value="' + division_id + '">' + division_name + '</option>'
    })
    return to_write
}

function hide_add_team() {
    reset_add_team()
    $('#add_team_subpanel').hide()
}

function reset_add_team() {
    current_add_id = 0
    $('#add_players').html('')
    $('#new_team_name').val('').trigger('propertychange')
}

function show_import_teams() {
    hide_import_teams()
    hide_add_team()

    $('#import_teams_subpanel').show()
}

function import_teams() {
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/import_teams/' + get_supertournament_id(),
        data: JSON.stringify({
            'teams':$('#new_teams').val()
        }),
        success: (function(response) {
            console.log('Import teams success.')  
            load_edit_data()
            if (response.message == 'MALFORMED DATA')
                alert('Malformed input! We tried our best to import your teams, but some of them may be off.')
            hide_import_teams()
        }),
        error: (function() {
            console.log('Import teams failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You imported team(s).',
                '400': 'There was a problem while importing team(s): ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to import team(s).',
                '403': 'You do not have permission to import team(s).',
                 '-1': 'There was an unknown problem while importing team(s). Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function hide_import_teams() {
    $('#import_teams_subpanel').hide()
}

function reset_import_teams() {
    $('#new_teams').val('')
    $('#new_teams').trigger('propertychange')
}

function add_new_player_input() {
    current_add_id += 1
    var to_write = ''
    to_write += '<div class="form-group floating-label-form-group col-sm-2">'
    to_write +=     '<label for="add_' + current_add_id + '">New player ' + current_add_id + '</label>'
    to_write +=     '<input type="text" id="add_' + current_add_id + '" class="form-control" placeholder="New player name">'
    to_write += '</div>'
    $('#add_players').append(to_write)
}

function add_team() {
    var players = []
    for (var i = 0; i < current_add_id; i++) {
        players.push(
            $('#add_' + (i + 1)).val()
        )
    }

    players = players.filter(function(e) { //Removes blank fields from list
        return $.trim(e)
    })

    var team_name = $.trim($('#new_team_name').val())

    var divisions = {}

    var division_fields = $('#new_team_division_form').serializeArray()
    $.each(division_fields, function(i, field) {
        var name = field.name.split('_')
        var bracket_id = name[5] //position of id in new_team_division_bracket_X
        divisions[bracket_id] = Number(field.value)
    })
    
    var team = {
        'name':team_name, 
        'players':players,
        'divisions':divisions
    }
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/add_team/' + get_supertournament_id(),
        data: JSON.stringify(team),
        success: (function(data) {
            console.log('Add team success.')  
            load_edit_data()
            
            hide_add_team()
        }),
        error: (function() {
            console.log('Add team failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You added a team.',
                '400': 'There was a problem while changing the tournament settings: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to add a team.',
                '403': 'You do not have permission to add a team.',
                 '-1': 'There was an unknown problem while adding a team. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function delete_team(team_id) {
    if (!confirm('Are you sure you want to delete the team? All data will be lost and this action cannot be undone.'))
        return

    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/delete_team/' + get_supertournament_id() + '/' + team_id,
        success: (function(data) {
            console.log('Delete team success.')  
            load_edit_data()
        }),
        error: (function() {
            console.log('Delete team failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You deleted a team.',
                '400': 'There was a problem while deleting a team: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to delete a team.',
                '403': 'You do not have permission to delete a team.',
                 '-1': 'There was an unknown problem while deleting a team. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function edit_teams() {
    var edits = $('#team_list_form :input').serializeArray()

    var team_data = {
        'player':{}, 
        'team':{}, 
        'new_player':{},
        'division':{}
    }

    $.each(edits, function(i, field) {
        var name = field.name.split('_')
        if (name[2] != 'new' && name[0] != 'division') { // Player or Team name change
            var type = name[0]
            var id = name[1]
            team_data[type][id] = field.value
        }
        else if (name[2] == 'new'){ //New player
            var type = 'new_player'
            var id = name[1]
            if (!(id in team_data[type]))
                team_data[type][String(id)] = []
            team_data[type][String(id)].push(field.value)
        }
        else { // Change division
            var type = name[0]
            var team_id = name[2]
            var bracket_id = name[4]
            if (!(team_id in team_data[type]))
                team_data[type][team_id] = {}
            team_data[type][team_id][bracket_id] = Number(field.value)
        }
    })

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/edit_teams/' + get_supertournament_id(),
        data: JSON.stringify(team_data),
        success: (function(data) {
            console.log('Edit teams success.')
            load_edit_data()
        }),
        error: (function() {
            console.log('Edit teams failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You edited teams.',
                '400': 'There was a problem while editing teams: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to edit teams.',
                '403': 'You do not have permission to edit teams.',
                 '-1': 'There was an unknown problem while editing teams. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function edit_teams_add_player(team_id) {
    //event.preventDefault()
    add_ids[team_id] += 1
    var name = 'team_' + team_id + '_new_' + add_ids[team_id]

    var to_write = ''
    to_write += '<div class="form-group floating-label-form-group col-sm-2">'
    to_write +=     '<label for="' + name + '">New player</label>'
    to_write +=     '<input type="text" name="' + name + '" id="' + name + '" class="form-control" placeholder="New player name">'
    to_write += '</div>'
    $('#team_' + team_id + '_players').append(to_write)
}

/* --------------------------
     Games and Brackets         
---------------------------*/
function load_brackets(brackets) {
    var to_write = ''
    $.each(brackets, function(id, bracket) {
        bracket_id = 'bracket_' + id
        round_id = 'round_' + bracket_id
        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12 col-md-12">'
        to_write +=         '<div class="form-group floating-label-form-group col-sm-3">'
        to_write +=             '<label for="' + bracket_id +'">Bracket name</label>'
        to_write +=             '<input type="text" name="' + bracket_id + '" id="' + bracket_id + '" class="form-control input-track-change" value="' + bracket['name'] + '" style="font-weight:bold;" placeholder="Bracket name">'
        to_write +=         '</div>'
        to_write +=         '<div class="form-group floating-label-form-group col-sm-3">'
        to_write +=             '<label for="' + round_id +'">Number of rounds</label>'
        to_write +=             '<input type="text" name="' + round_id + '" id="' + round_id + '" class="form-control input-track-change" value="' + bracket['n_round'] + '" placeholder="Number of rounds">'
        to_write +=         '</div>'
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12">'
        to_write +=         '<div id="divisions_' + bracket_id + '">'
        to_write +=             load_divisions(bracket['divisions'], bracket_id)
        to_write +=         '</div>'
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12">'
        to_write +=         '<small><a href="javascript:void(0);" onClick="add_new_division_input(' + id + ')">[Add a division]</a></small>'
        to_write +=         '&mdash;'
        to_write +=         '<small><a href="javascript:void(0);" onClick="show_add_game(' + id + ')">[Add a game]</a></small>'
        to_write +=         '&mdash;'
        to_write +=         '<small><a href="javascript:void(0);" onClick="delete_bracket(' + id + ')">[Delete this bracket]</a></small>'
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<br>'
        to_write += '<div class="row">'
        to_write +=     '<div class="col-sm-12">'
        to_write +=         load_games(edit_data['rounds'], id)
        to_write +=     '</div>'
        to_write += '</div>'
        to_write += '<hr>'
    })

    $('#edit_brackets').html(to_write)
    
    $.each(brackets, function(id, bracket) {
        bracket_id = 'bracket_' + id
        round_id = 'round_' + bracket_id
        $('#' + bracket_id).trigger('propertychange')
        $('#' + round_id).trigger('propertychange')
        $.each(bracket['divisions'], function(id, division) {
            division_id = 'division_' + id + '_' + bracket_id
            $('#' + division_id).trigger('propertychange')
        })
    })
}

function load_divisions(divisions, bracket_id) {
    var to_write = ''
    $.each(divisions, function(id, division_name) {
        division_id = 'division_' + id + '_' + bracket_id
        
        to_write += '<div class="form-group floating-label-form-group col-sm-2">'
        to_write +=     '<label for="' + division_id + '">Division</label>'
        to_write +=     '<input type="text" name="' + division_id + '" id="' + division_id + '" class="form-control input-track-change" value="' + division_name + '" placeholder="Division name">'
        to_write += '</div>'
    })

    return to_write
}

function add_new_division_input(bracket_id) {
    current_add_division_id += 1
    division_id = 'add_division_' + current_add_division_id + '_bracket_' + bracket_id
    var to_write = ''
    to_write += '<div class="form-group floating-label-form-group col-sm-2">'
    to_write +=     '<label for="' + division_id + '">New division</label>'
    to_write +=     '<input type="text" name="' + division_id + '" id="' + division_id + '" class="form-control input-track-change" placeholder="New division name">'
    to_write += '</div>'
    $('#divisions_bracket_' + bracket_id).append(to_write)
}

function load_games(game_rounds, bracket_id) {
    var to_write = ''
    
    $.each(game_rounds[bracket_id], function(id, round) {
        to_write += '<b>Round ' + id + '</b>'
        $.each(round, function(id, game) {
            to_write += '<br>' 
            to_write += '<small>'
            to_write += game[0]['team_name'] + ' ' + game[0]['total']
            to_write += ' v. '
            to_write += game[1]['team_name'] + ' ' + game[1]['total']
            to_write += '&nbsp;'
            to_write += '<a href="#" onClick="delete_game(' + bracket_id + ', ' + game['id'] + ')">(delete)</a>' 
            to_write += '</small>'
        })
        to_write += '<br>'
    })

    return to_write    
}

function add_bracket() {
    bracket = {
        'name': $('#add_bracket_name').val(),
        'n_round': Number($('#add_bracket_n_round').val())
    }
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/add_bracket/' + get_supertournament_id(), 
        data: JSON.stringify(bracket),
        success: (function(data) {
            console.log('Add bracket success.')
            load_edit_data()
            $('#add_bracket_form')[0].reset()
            $('#add_bracket_name').trigger('propertychange')
            $('#add_bracket_n_round').trigger('propertychange')
        }),
        error: (function() {
            console.log('Add bracket failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You added a bracket.',
                '400': 'There was a problem while adding a bracket: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to add a bracket.',
                '403': 'You do not have permission to add a bracket.',
                 '-1': 'There was an unknown problem while adding a bracket. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function edit_brackets() {
    var brackets_data = {
        'bracket':{},
        'round':{},
        'division':{},
        'add_division':{}
    }
    
    brackets = $('#brackets_form').serializeArray()
    $.each(brackets, function(i, field) {
        var name = field.name.split('_')
        if (name[0] == 'bracket') {
            brackets_data['bracket'][name[1]] = field.value 
        }
        else if (name[0] == 'round') {
            brackets_data['round'][name[2]] = Number(field.value) // bracket.id:number of rounds
        }
        else if (name[0] == 'division') {
            bracket_id = name[3]
            division_id = name[1]
            if (!(bracket_id in brackets_data['division']))
                brackets_data['division'][bracket_id] = {}
            brackets_data['division'][bracket_id][division_id] = field.value
        }
        else {
            if (!(bracket_id in brackets_data['add_division']))
                brackets_data['add_division'][bracket_id] = []
            brackets_data['add_division'][bracket_id].push(field.value)
        }
    })
    console.log(brackets_data)

    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/edit_brackets/' + get_supertournament_id() + '/args',
        data: JSON.stringify(brackets_data),
        success: (function(data) {
            console.log('Edit brackets success.')
            load_edit_data()
        }),
        error: (function(response) {
            console.log('Edit brackets failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You edited your brackets.',
                '400': 'There was a problem while changing the brackets: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to change brackets.',
                '403': 'You do not have permission to change the brackets.',
                 '-1': 'There was an unknown problem while changing the brackets. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function delete_bracket(bracket_id) {
    if (!confirm('Are you sure you want to delete this bracket? All games will be lost but the teams will be saved.'))
        return
    
    $.ajax({
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/delete_bracket/' + get_supertournament_id() + '/' + bracket_id, 
        success: (function(data) {
            console.log('Delete bracket success.')
            load_edit_data()
        }),
        error: (function() {
            console.log('Delete bracket failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You deleted a bracket.',
                '400': 'There was a problem while deleting a bracket: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to delete a bracket.',
                '403': 'You do not have permission to delete a bracket.',
                 '-1': 'There was an unknown problem while deleting a bracket. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function show_add_game(bracket_id) {
    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/synchronize/' + get_supertournament_id() + '/' + bracket_id, 
        success: (function(data) {
            console.log('Synchronize for add game success.')
            game_sync_data = data
            
            var rounds = data['rounds']
            var round_select = '<select id="game_round_select" class="form-control">'
             round_select += '<option value="-1">Choose a round</option>'
            $.each(rounds, function(round_id, round_number) {
                round_select += '<option value="' + round_id + '" label="' + edit_data['brackets'][bracket_id]['name'] + ' Bracket, Round ' + round_number + '"></option>'
            })
            round_select += '</select>'
            $('#round_select_panel').html(round_select)
       
            var teams = data['teams']
            var team_select = ''
            for (var i = 0; i < 2; i++) {
                var team_type = ['red', 'blu'][i]
                var team_select = '<select id="' + team_type + '_select" class="form-control" onChange="add_game_populate_remote_players(\'' + team_type + '\')">'
                team_select += '<option value="-1">Choose a team</option>'
                $.each(teams, function(team_id, team) {
                    team_select += '<option value="' + team_id + '" label="' + team['name'] + '"></option>'
                })
                team_select += '</select>'
                $('#' + team_type + '_select_panel').html(team_select)
            }
        
            add_game_bracket_id = bracket_id    
            $('#add_game_subpanel').show()
        }),
        error: (function(){console.log('Synchronize for add game failure.')})
    })
}

function hide_add_game() {
    $('#new_game_form')[0].reset()
    $('#game_red_players').html('')
    $('#game_blu_players').html('')
    $('#add_game_subpanel').hide()
}

function add_game() {
    var n_round = game_sync_data['rounds'][$('#game_round_select').val()]
    var scores = {'teams':{}, 'players':{}, 'n_round':Number(n_round)}

    for (var i = 0; i < 2; i++) {
        var team_type = ['red', 'blu'][i]
        var team_id = $('#' + team_type + '_select').val()
        var team_score = $('#' + team_type + '_game_total').val()
        var team_heard = $('#' + team_type + '_heard').val()
        scores['teams'][team_id] = {
            'total':Number(team_score),
            'heard':Number(team_heard)
        }
    }

    $.each($('#new_game_form :input').serializeArray(), function(i, field) {
        var name = field.name.split('_')
        var id = name[0]
        var type = name[1]
        if (!(id in scores['players']))
            scores['players'][id] = {}
        scores['players'][id][type] = Number(field.value)
    })
    
    $.ajax({
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json',
        url: SCRIPT_ROOT + '/add_game/' + get_supertournament_id() + '/' + add_game_bracket_id,
        data: JSON.stringify(scores),
        success: (function(data) {
            console.log('Add game success.')
            load_edit_data()
        }),
        error: (function() {
            console.log('Add game failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You added a game.',
                '400': 'There was a problem while adding a game: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to add a game.',
                '403': 'You do not have permission to add a game.',
                 '-1': 'There was an unknown problem while adding a game. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}

function add_game_populate_remote_players(team_type) {
    team_id = $('#' + team_type + '_select').val()
    if (team_id == '-1') {
        $('#game_' + team_type + '_players').html('')
        return;
    }
    
    var team = game_sync_data['teams'][team_id]

    var player_statistics = [['heard', 'Tossups heard'], ['power', 'Powers'], ['tossup', 'Tossups'], ['negative', 'Negatives']]
        
    to_write = '' + //Should probably be in HTML.
    '<div class="row">' +
        '<form role="form" class="form-inline">' +
            '<div class="col-sm-12">' +
                '<div class="form-group">' +
                    '<div class="col-sm-2">' +
                        '<input type="text" class="form-control" id="' + team_type + '_game_total" placeholder="Team total">' +
	            '</div>' +
                    '<div class="col-sm-2">' +
                        '<input type="text" class="form-control" id="' + team_type + '_heard" placeholder="Team tossups heard">' +
	            '</div>' +
                '</div>' +
            '</div>' +
        '</form>' + 
    '</div>'

    to_write += '' + 
    '<div class="row">' +
        '<div class="col-sm-12">' +
            '<form role="form">'

    var i = 0
    $.each(team['players'], function(player_id, name) {
        to_write += '<div class="form-group col-sm-2" style="text-align:center;">'
        to_write +=     '<span> <b>' + name + '</b> </span>'
        for (var j = 0; j < player_statistics.length; j++) {
            statistic_id = player_id + '_' + player_statistics[j][0]
            to_write += '   <input type="text" class="form-control" name="' + statistic_id + '" id="' + statistic_id + '" style="margin-bottom:2px" placeholder="' + player_statistics[j][1] + '">'
        }
        to_write += '</div>'

        if ((i + 1) % 6 == 0) {
            to_write += '' + 
                    '</form>' + 
                '</div>' +
            '</div>'
            to_write += '' + 
            '<br>' + 
            '<div class="row">' +
                '<div class="col-sm-12">' +
                    '<form role="form">'
        }
        i++
    })

    to_write += '' + 
            '</form>' + 
        '</div>' +
    '</div>'
    
    $('#game_' + team_type + '_players').html(to_write)
}

function delete_game(bracket_id, game_id) {
    if (!confirm('Are you sure you want to delete this game? All data will be permanently lost.'))
        return

    $.ajax({
        datatype: "json", 
        url: SCRIPT_ROOT + '/delete_game/' + get_supertournament_id() + '/' + bracket_id + '/' + game_id,
        success: (function(data) {
            console.log('Delete game success.')
            load_edit_data()
        }),
        error: (function() {
            console.log('Delete game failure.')
        }),
        complete: (function(response) {
            parse_response(response, {
                '200': 'You deleted a game.',
                '400': 'There was a problem while deleting a game: ' + (response && response.responseJSON && response.responseJSON.error_message || ''),
                '401': 'You need to be logged in to delete a game.',
                '403': 'You do not have permission to delete a game.',
                 '-1': 'There was an unknown problem while deleting a game. Try again and then submit a bug report.'
            }, 'edit')
        })
    })
}
