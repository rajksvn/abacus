**Abacus**, a web application for quizbowl scorekeeping.

Abacus version control has moved to https://github.com/raj-kesavan/abacus. Thanks!