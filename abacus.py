####
# Abacus, a web application for quizbowl moderators
# Balachandar Kesavan, November 2013-April 2014
####

import os, sys, subprocess, time, string, random, operator, requests

from flask import Flask, render_template, request, redirect, url_for, send_from_directory, jsonify, session, abort, json, g

from werkzeug import secure_filename
from flask.ext.mail import Mail, Message
from flask.ext.sqlalchemy import SQLAlchemy, event

from voluptuous import Schema, Required, All, Length, Range, Invalid, MultipleInvalid, Match, Coerce

import types

import abacus_settings

app = Flask(__name__)

app.config.from_object('abacus.abacus_settings')

if not app.debug:
    import logging
    from logging import FileHandler
    file_handler = FileHandler(app.config['ERROR_FILE'])
    file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)

###############################
##### DATABASE SCHEMATICS #####
###############################
db = SQLAlchemy(app)

director = db.Table('director',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('supertournament_id', db.Integer, db.ForeignKey('supertournament.id'))
)

moderator = db.Table('moderator',
    db.Column('user_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('supertournament_id', db.Integer, db.ForeignKey('supertournament.id'))
)

team_division = db.Table('team_division',
   db.Column('team_id', db.Integer, db.ForeignKey('team.id')), 
   db.Column('division_id', db.Integer, db.ForeignKey('division.id'))
)

class User(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    email = db.Column(db.String)

    directed_supertournaments = db.relationship('Supertournament', secondary = director, backref = db.backref('directed_supertournaments', lazy = 'dynamic'))
    moderated_supertournaments = db.relationship('Supertournament', secondary = moderator, backref = db.backref('moderated_supertournaments', lazy = 'dynamic'))

    def __init__(self, email):
        self.email = email

    def __repr__(self):
        return '<User {0}>'.format(self.email)

class Supertournament(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    html = db.Column(db.String)

    teams = db.relationship('Team', backref = 'supertournament', lazy = 'dynamic', cascade = 'all, delete-orphan')

    tournaments = db.relationship('Tournament', backref = 'supertournament', lazy = 'dynamic', cascade = 'all, delete-orphan')
    
    directors = db.relationship('User', secondary = director, backref = db.backref('directors', lazy = 'dynamic'))
    moderators = db.relationship('User', secondary = moderator, backref = db.backref('moderators', lazy = 'dynamic'))
    
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Supertournament {0}>'.format(self.name)

class Tournament(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    html = db.Column(db.String)
    supertournament_id = db.Column(db.Integer, db.ForeignKey('supertournament.id'))

    rounds = db.relationship('Round', backref = 'tournament', lazy = 'dynamic', cascade = 'all, delete-orphan')
    divisions = db.relationship('Division', backref = 'tournament', lazy = 'dynamic', cascade = 'all, delete-orphan')
    
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Tournament {0}>'.format(self.name)

class Round(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    number = db.Column(db.Integer)
    tournament_id = db.Column(db.Integer, db.ForeignKey('tournament.id'))

    games = db.relationship('Game', backref = 'round', lazy = 'dynamic', cascade = 'all, delete-orphan')

    def __init__(self, number):
        self.number = number

    def __repr__(self):
        return '<Round {0}>'.format(self.number)

class Game(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    round_id = db.Column(db.Integer, db.ForeignKey('round.id'))

    playerScores = db.relationship('PlayerScore', backref = 'game', lazy = 'dynamic', cascade = 'all, delete-orphan')
    teamScores = db.relationship('TeamScore', backref = 'game', lazy = 'dynamic', cascade = 'all, delete-orphan')

    def __init__(self):
        pass

    def __repr__(self):
        return '<Game>'

class Division(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    tournament_id = db.Column(db.Integer, db.ForeignKey('tournament.id'))
    
    teams = db.relationship('Team', secondary = team_division, backref = db.backref('teams', lazy = 'dynamic'))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Division {0}>'.format(self.name)

class Team(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    supertournament_id = db.Column(db.Integer, db.ForeignKey('supertournament.id'))

    divisions = db.relationship('Division', secondary = team_division, backref = db.backref('divisions', lazy = 'dynamic'))

    players = db.relationship('Player', backref = 'team', lazy = 'dynamic', cascade = 'all, delete-orphan')
    teamScores = db.relationship('TeamScore', backref = 'team', lazy = 'dynamic', cascade = 'all, delete-orphan')

    def __init__(self, name, tournament_id = None, division_id = None):
        self.name = name

        if tournament_id is not None:
            self.tournament_id = game_id

        if division_id is not None:
            self.division_id = division_id

    def __repr__(self):
        return '<Team {0}>'.format(self.name)

class Player(db.Model):
    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String)
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'))

    playerScores = db.relationship('PlayerScore', backref = 'player', lazy = 'dynamic', cascade = 'all, delete-orphan')

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Player {0}>'.format(self.name)

class PlayerScore(db.Model):
    n_power = db.Column(db.Integer)
    n_tossup = db.Column(db.Integer)
    n_negative = db.Column(db.Integer)
    n_heard = db.Column(db.Integer)
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), primary_key = True)
    player_id = db.Column(db.Integer, db.ForeignKey('player.id'), primary_key = True)

    def __init__(self, n_power, n_tossup, n_negative, n_heard, game_id = None, player_id = None):
        self.n_power = n_power
        self.n_tossup = n_tossup
        self.n_negative = n_negative
        self.n_heard = n_heard

        if game_id is not None:
            self.game_id = game_id

        if player_id is not None:
            self.player_id = player_id

    def __repr__(self):
        return '<PlayerScore {0}, {1}, {2}, {3}>'.format(self.n_power, self.n_tossup, self.n_negative, self.n_heard)

class TeamScore(db.Model):
    bonus_total = db.Column(db.Integer)
    n_heard = db.Column(db.Integer)
    game_id = db.Column(db.Integer, db.ForeignKey('game.id'), primary_key = True)
    team_id = db.Column(db.Integer, db.ForeignKey('team.id'), primary_key = True)

    def __init__(self, bonus_total, n_heard, game_id = None, team_id = None):
        self.bonus_total = bonus_total
        self.n_heard = n_heard

        if game_id is not None:
            self.game_id = game_id

        if team_id is not None:
            self.team_id = team_id

    def __repr__(self):
        return '<TeamScore {0}, {1}>'.format(self.bonus_total, self.n_heard)

###############################
########### ROUTING ###########
###############################
@app.before_request
def get_current_user():
    g.user = None
    email = session.get('email')
    if email is not None:
        g.user = email

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/create')
def create():
    if g.user is None:
        return render_template('error.html', error_code = '401', error = 'You need to be logged in to create a tournament.')

    return render_template('create.html')

@app.route('/loaded/<int:supertournament_id>')
@app.route('/loaded/<int:supertournament_id>/<int:tournament_id>')
def loaded(supertournament_id = 0, tournament_id = 0):
    g.supertournament_id = supertournament_id

    supertournament = Supertournament.query.get(supertournament_id)

    if supertournament is None:
        return render_template('error.html', error_code = '404', error = 'This tournament does not exist.')

    if tournament_id != 0:
        tournament = Tournament.query.get(tournament_id)
        if not tournament or tournament not in supertournament.tournaments:
            return render_template('error.html', error_code = '404', error = 'This tournament does not exist.')

    g.supertournament = supertournament.name
    
    if tournament_id == 0:
        return render_template('blank.html', statistics = supertournament.html)
    else:
        return render_template('blank.html', statistics = tournament.html)

@app.route('/edit/<int:supertournament_id>')
def edit(supertournament_id = 0):
    if g.user is None:
        return render_template('error.html', error_code = '401', error = 'You need to be logged in to edit a tournament.')

    g.supertournament_id = supertournament_id

    supertournament = Supertournament.query.get(supertournament_id)

    if not supertournament:
        return render_template('error.html', error_code = '404', error = 'This tournament does not exist.')

    if has_credential(supertournament.id, g.user, 'director'):
        g.supertournament = supertournament.name 
        return render_template('edit.html', 
            supertournament_id = supertournament.id, 
            tournaments = generate_statistic_report_titles(supertournament)
        )
    else:
        return render_template('error.html', error_code = '403', error = 'You don\'t have permission to edit this tournament.')

@app.route('/account')
def account():
    if g.user is None:
        return render_template('error.html', error_code = '401', error = 'You need to be logged in to access myAbacus.')
    
    user = User.query.filter_by(email = g.user).first()
    return render_template(
        'account.html',
        directed_tournaments = user.directed_supertournaments,
        moderated_tournaments = user.moderated_supertournaments
    )

##############################
########### ERRORS ###########
##############################
@app.errorhandler(403)
def not_authorized(e):
    response = jsonify({'code':403, 'message':'NOT_AUTHORIZED'})
    response.status_code = 403
    return response

@app.errorhandler(401)
def not_authenticated(e):
    response = jsonify({'code':401, 'message':'NOT_AUTHENTICATED'})
    response.status_code = 401
    return response

@app.errorhandler(400)
def bad_request(e):
    response = jsonify({'code':400, 'message':'BAD_REQUEST'})
    response.status_code = 400
    return response

##############################
##### MODERATOR CONTROL ######
##############################
@app.route('/upload_packet', methods=['POST', 'GET'])
def upload_packet():
    file = request.files['file']

    if file and allowed_file(file.filename):
        pkt_filename = ''.join(random.choice(string.ascii_uppercase + string.digits) for x in range(10)) + '.' + file.filename.rsplit('.', 1)[1]
        file.save(os.path.join(app.config['UPLOAD_FOLDER'], pkt_filename))

    return jsonify({'filename':'/docs/' + pkt_filename})

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1] in app.config['ALLOWED_EXTENSIONS']

@app.route('/upload_database/<int:supertournament_id>/<int:tournament_id>', methods = ['POST'])
def upload_database(supertournament_id = 0, tournament_id = 0):
    credential_check(supertournament_id, 'moderator')

    data = request.json
    
    schema = Schema({
        Required('n_round'): All(int, Range(min = 1)),
        Required('teams'): [
            {
                Required('id'): int,
                Required('bonus_total'): int,
                Required('heard'): int,
                Required('players'): [
                    {
                        Required('id'): int,
                        Required('15'): int,
                        Required('10'): int,
                        Required('n5'): int,
                        Required('heard'): int
                    }
                ]
            }
        ]
    }, extra = True) #Allows for additional unnecessary key like player name, etc.

    try:
        schema(data)
    except (Invalid, MultipleInvalid) as e:
        abort(400)

    supertournament = Supertournament.query.get(supertournament_id)
    tournament = Tournament.query.get(tournament_id)
    
    if not supertournament or not tournament:
        abort(404)
         
    if tournament not in supertournament.tournaments:
        abort(403)
    
    round = Round.query.filter_by(tournament_id = tournament.id).filter_by(number = data['n_round']).first()
    
    if not round:
        abort(404)

    if round not in tournament.rounds:
        abort(403)
    
    game = Game()
    round.games.append(game)
    db.session.commit()

    for local_team in data['teams']:
        team = Team.query.get(local_team['id'])
        if not team:
            abort(404)
        if team not in supertournament.teams:
            abort(403)
        for local_player in local_team['players']:
            player = Player.query.get(local_player['id'])
            if not player:
                abort(404)
            if player not in team.players:
                abort(403)
            playerScore = PlayerScore(
                local_player['15'],
                local_player['10'],
                local_player['n5'],
                local_player['heard'],
                game_id = game.id
            )
            player.playerScores.append(playerScore)

        teamScore = TeamScore(
            local_team['bonus_total'],
            local_team['heard'],
            game_id = game.id
        )
        team.teamScores.append(teamScore)

    db.session.commit()

    return jsonify({'message':'Successfully uploaded to database'})

@app.route('/populate_synchronize')
def populate_synchronize():
    brackets = {}
    user = User.query.filter_by(email = g.user).first()
    
    raw_supertournaments = user.directed_supertournaments + user.moderated_supertournaments    

    for supertournament in raw_supertournaments:
        for tournament in supertournament.tournaments:
            brackets[tournament.id] = {
                'supertournament_id':supertournament.id, 
                'supertournament_name':supertournament.name,
                'bracket_name':tournament.name
            }

    return jsonify({'brackets':brackets})

@app.route('/synchronize/<int:supertournament_id>/<int:tournament_id>')
def synchronize(supertournament_id = 0, tournament_id = 0):
    credential_check(supertournament_id, 'moderator')

    supertournament = Supertournament.query.get(supertournament_id)
    tournament = Tournament.query.get(tournament_id)

    if not supertournament or not tournament:
        abort(404)

    if tournament not in supertournament.tournaments:
        abort(403)
    
    rounds = {}
    for round in tournament.rounds.order_by(Round.number):
        rounds[round.id] = round.number

    teams = {}
    for team in supertournament.teams.order_by(Team.name):
        players = {}

        for player in team.players:
            players[player.id] = player.name

        teams[team.id] = {'name':team.name, 'players':players}

    return jsonify({
        'tournament':supertournament.name,
        'bracket':tournament.name, 
        'moderator':g.user, 
        'rounds':rounds, 
        'teams':teams
    })

###############################
###### CREATE TOURNAMENT ######
###############################
@app.route('/create_tournament', methods = ['POST'])
def create_tournament():
    supertournament_name = request.json['tournament'] 

    supertournament = Supertournament(supertournament_name)
    db.session.add(supertournament)
    db.session.commit()

    user = User.query.filter_by(email = g.user).first()

    supertournament.directors.append(user)
    
    bracket_names = request.json['brackets']

    for bracket_name in bracket_names:
        tournament = Tournament(bracket_name)
        supertournament.tournaments.append(tournament)
        db.session.commit()

        tournament.divisions.append(Division('Main Division'))
         
        for i in range(0, 5):
            tournament.rounds.append(Round(i + 1))

    db.session.commit()

    return jsonify({'supertournament_id':supertournament.id})

##############################:
###### LOAD TOURNAMENT #######
##############################
#Generates html for the selected supertournament
@app.route('/update_statistics/<int:supertournament_id>')
def update_statistics(supertournament_id = 0):
    supertournament = Supertournament.query.get_or_404(supertournament_id)
    queue_statistics_update(supertournament)
    return jsonify({'message':'Updated statistics! :)'})

#Use something like RQ
def queue_statistics_update(supertournament):
    generate_statistics(supertournament, None) 
    for tournament in supertournament.tournaments:
        generate_statistics(supertournament, tournament)

#Generates html for specified bracket. if tournament is none, then calculates for entire supertournament ('Overall'). 
def generate_statistics(supertournament, tournament = None):
    if not tournament:
        other_brackets = supertournament.tournaments.all()
    else:
        other_brackets = supertournament.tournaments.filter(Tournament.id != tournament.id).all()
   
    links_list = []
    
    for other_bracket in other_brackets:
         links_list.append(
            '<a href="/loaded/' + str(supertournament.id) + '/' + str(other_bracket.id) + '">'
            + other_bracket.name
            + '</a>'
         )

    #Links is a list of other brackets in the tournament, includes 'Overall' if someone is in a specific bracket.
    links = ' - '.join(links_list)

    if tournament:
        links += ' - <a href="/loaded/' + str(supertournament.id) + '">Overall</a>'

    #It's okay if tournament is None in case of Overall - following functions take that to indicate Overall calculations.
    individual_data = individuals(tournament, supertournament)
    team_detail_data = team_detail(tournament, individual_data, supertournament)

    if not tournament:
        bracket_name = 'Overall'
    else:
        bracket_name = tournament.name

    rendered = render_template(
        'statistics.html', 
        #Statistics data structures
        scoreboards = scoreboard(tournament, supertournament),
        standings = standings(tournament, team_detail_data, supertournament),
        individuals = individual_data,
        individual_details = individual_detail(tournament, individual_data, supertournament),
        team_details = team_detail_data,
        round_reports = round_report(tournament, supertournament),
        #Auxiliary information
        supertournament_name = supertournament.name,
        bracket_name = bracket_name,
        other_brackets = links
    ) 
    
    if not tournament:
        supertournament.html = rendered
    else: 
        tournament.html = rendered

    db.session.commit()

#Rounds a value, then forces it to have decimals number of digits after the decimal point.
@app.template_filter('force_decimal')
def force_decimal(value, decimals = 2):
    d = "%0.{0}f".format(decimals) % value
    if d != '-1.00':
        return d
    return ''

def scoreboard(tournament, supertournament):
    scores = []

    if tournament:
        tournament_rounds = tournament.rounds
    else:
        rounds = [tournament.rounds.order_by(Round.number).all() for tournament in supertournament.tournaments.order_by(Tournament.id)]
        tournament_rounds = [round for bracket in rounds for round in bracket]

    for round in tournament_rounds:
        round_scores = {
            'id': round.id,
            'number': round.number,
            'bracket': round.tournament.name,
            'games':[]
        }

        for game in round.games:
            game_scores = []
            
            for teamScore in game.teamScores:
                team = teamScore.team
                
                player_scores = []

                for player in teamScore.team.players:
                    playerScore = game.playerScores.filter_by(player_id = player.id).first()
                    
                    player_scores.append({
                        'name': player.name,
                        'n_power': playerScore.n_power,
                        'n_tossup': playerScore.n_tossup,
                        'n_negative': playerScore.n_negative,
                        'n_heard': playerScore.n_heard,
                        'total':15 * playerScore.n_power + 10 * playerScore.n_tossup - 5 * playerScore.n_negative
                    })
                    
                game_scores.append({
                    'name': team.name,
                    'total': get_total_score(teamScore),
                    'points_per_bonus': get_points_per_bonus(teamScore),
                    'players': player_scores
                })

            round_scores['games'].append(game_scores)
        scores.append(round_scores)
    return scores

def standings(tournament, team_detail_data, supertournament):
    standings = []
    
    if not tournament:
        divisions = [0] #Dummy list so it can iterate through. Since it is Overall, there is only one division with all the teams.
    else:
        divisions = tournament.divisions
     
    for division in divisions:
        division_scores = {
            'teams': []
        }
        
        if not tournament:
            division_scores['name'] = 'Overall'
            teams = supertournament.teams
        else:
            division_scores['name'] = division.name
            teams = division.teams

        for team in teams:
            team_statistics = {
                'name': team.name,
                'win': 0,
                'loss': 0,
                'tie': 0,
                'points_for': 0,
                'points_against': 0
            }

            if not tournament:
                teamScores = team.teamScores.all()
            else:
                teamScores = get_relevant_teamScores(team, tournament)
            
            team_statistics['games_played'] = len(teamScores)
            
            for teamScore in teamScores: 
                game = teamScore.game
                opponent_teamScore = game.teamScores.filter(TeamScore.team_id != team.id).first()

                total_for = get_total_score(teamScore)
                total_against = get_total_score(opponent_teamScore)

                if total_for > total_against: 
                    team_statistics['win'] += 1
                elif total_for < total_against: 
                    team_statistics['loss'] += 1
                else:
                    team_statistics['tie'] += 1
                
                team_statistics['points_for'] += total_for 
                team_statistics['points_against'] += total_against
            
            team_statistics['win_ratio'] = divide(team_statistics['win'], team_statistics['games_played'])
            team_statistics['points_per_game'] = divide(team_statistics['points_for'], team_statistics['games_played'])
            team_statistics['opponent_points_per_game'] = divide(team_statistics['points_against'], team_statistics['games_played'])
            team_statistics['margin'] = team_statistics['points_per_game'] - team_statistics['opponent_points_per_game']
            
            for team_detail in team_detail_data:
                if team_detail['id'] == team.id:
                    for statistic_name in team_detail['totals']:
                        team_statistics[statistic_name] = team_detail['totals'][statistic_name]

            division_scores['teams'].append(team_statistics)

        division_scores['teams'] = sorted(division_scores['teams'], key = lambda k:(-k['win'], k['loss'], -k['points_per_game']))
        standings.append(division_scores)

    return standings

def individuals(tournament, supertournament):
    scores = []

    for team in supertournament.teams:
        for player in team.players:
            if not tournament:
                division_name = 'Overall'
            else:
                division_name = get_unique_division(tournament, player.team).name

            statistics = {
                'games_played': 0,
                'name': player.name,
                'team': player.team.name,
                'division': division_name, 
                'n_heard': 0,
                'n_power': 0,
                'n_tossup': 0,
                'n_negative': 0,
                'total': 0,
                'team_id': team.id, #For team detail select
                'player_id': player.id #For individual detail select
            }
            
            if not tournament: #Calculating overall stats
                teamScores = team.teamScores
                playerScores = player.playerScores
            else: #Calculating stats for a single bracket
                teamScores = get_relevant_teamScores(team, tournament)
                playerScores = get_relevant_playerScores(player, tournament)

            for teamScore in teamScores: 
                game = teamScore.game
                playerScore = game.playerScores.filter_by(player_id = player.id).first()
                statistics['games_played'] += divide(playerScore.n_heard, teamScore.n_heard)

            for playerScore in playerScores:
                statistics['n_heard'] += playerScore.n_heard
                statistics['n_power'] += playerScore.n_power
                statistics['n_tossup'] += playerScore.n_tossup
                statistics['n_negative'] += playerScore.n_negative
                statistics['total'] += 15 * playerScore.n_power + 10 * playerScore.n_tossup - 5 * playerScore.n_negative

            statistics['points_per_game'] = divide(statistics['total'], statistics['games_played'])
            statistics['points_per_tossup'] = divide(statistics['total'], statistics['n_heard'])
            statistics['gets_per_negative'] = divide(statistics['n_power'] + statistics['n_tossup'], statistics['n_negative'])
            statistics['powers_per_negative'] = divide(statistics['n_power'], statistics['n_negative'])

            scores.append(statistics)

    return sorted(scores, key = lambda k:(-k['points_per_game'], -k['games_played']))

def team_detail(tournament, individual_data, supertournament):
    scores = []

    for team in supertournament.teams:
        team_scores = {
            'name': team.name,
            'id': team.id,
            'games': [],
            'totals': {},
            'individuals': []
        }
        
        if not tournament:
            teamScores = team.teamScores
        else:
            teamScores = get_relevant_teamScores(team, tournament)
        
        #Sort games by bracket id first, then round number
        sorted_teamScores = sorted(teamScores, key = lambda k: (k.game.round.tournament.id, k.game.round.number))
        
        for teamScore in sorted_teamScores:
            game = teamScore.game
            opponent_teamScore = game.teamScores.filter(TeamScore.team_id != team.id).first()
            
            game_scores = {
                'opponent_name': opponent_teamScore.team.name,
                'total': get_total_score(teamScore),
                'opponent_total': get_total_score(opponent_teamScore),
                'n_power': 0,
                'n_tossup': 0,
                'n_negative': 0,
                'player_total': 0
            }

            if game_scores['total'] > game_scores['opponent_total']:
                game_scores['result'] = 'WIN'
            elif game_scores['total'] < game_scores['opponent_total']:
                game_scores['result'] = 'LOSS'
            else:
                game_scores['result'] = 'TIE'

            statistics = ['n_power', 'n_tossup', 'n_negative']
            for player in team.players:
                playerScore = player.playerScores.filter_by(game_id = game.id).first()
                game_scores['n_power'] += playerScore.n_power
                game_scores['n_tossup'] += playerScore.n_tossup
                game_scores['n_negative'] += playerScore.n_negative

            game_scores['n_heard'] = teamScore.n_heard
            game_scores['player_total'] = 15 * game_scores['n_power'] + 10 * game_scores['n_tossup'] - 5 * game_scores['n_negative']
            game_scores['points_per_tossup'] = divide(game_scores['total'], game_scores['n_heard'])
            game_scores['powers_per_negative'] = divide(game_scores['n_power'], game_scores['n_negative'])
            game_scores['gets_per_negative'] = divide(game_scores['n_power'] + game_scores['n_tossup'], game_scores['n_negative'])
            game_scores['bonuses_heard'] = game_scores['n_power'] + game_scores['n_tossup']
            game_scores['bonus_points'] = game_scores['total'] - game_scores['player_total']
            game_scores['points_per_bonus'] = divide(game_scores['bonus_points'], game_scores['bonuses_heard'])

            team_scores['games'].append(game_scores)

        totals = {}

        #statistics that we can simply combine by adding
        statistics = ['total', 'opponent_total', 'n_power', 'n_tossup', 'n_negative', 'n_heard', 'bonuses_heard', 'bonus_points']

        for statistic in statistics:
            totals[statistic] = 0

        for game in team_scores['games']:
            for statistic in statistics:
                totals[statistic] += game[statistic]

        totals['points_per_tossup'] = divide(totals['total'], totals['n_heard'])
        totals['powers_per_negative'] = divide(totals['n_power'], totals['n_negative'])
        totals['gets_per_negative'] = divide(totals['n_power'] + totals['n_tossup'], totals['n_negative'])
        totals['points_per_bonus'] = divide(totals['bonus_points'], totals['bonuses_heard'])

        team_scores['totals'] = totals

        individuals = []
        for individual in individual_data:
            if individual['team_id'] == team.id:
                individuals.append(individual)

        team_scores['individuals'] = individuals

        scores.append(team_scores)

    return scores

def individual_detail(tournament, individual_data, supertournament):
    scores = []

    for team in supertournament.teams.order_by(Team.name).all():
        for player in team.players.order_by(Player.name):
            statistics = {
                'name': player.name,
                'team': player.team.name,
                'games': [],
                'totals': {}
            }

            if not tournament:
                playerScores = player.playerScores
            else:
                playerScores = get_relevant_playerScores(player, tournament)

            for playerScore in playerScores:
                teamScore = playerScore.game.teamScores.filter_by(team_id = team.id).first()
                opponent_teamScore = playerScore.game.teamScores.filter(TeamScore.team_id != team.id).first()

                game_statistics = {
                    'opponent_name': opponent_teamScore.team.name,
                    'games_played': divide(playerScore.n_heard, teamScore.n_heard),
                    'n_heard': playerScore.n_heard,
                    'n_power': playerScore.n_power,
                    'n_tossup': playerScore.n_tossup,
                    'n_negative': playerScore.n_negative,
                    'total': 15 * playerScore.n_power + 10 * playerScore.n_tossup - 5 * playerScore.n_negative,
                    'powers_per_negative': divide(playerScore.n_power, playerScore.n_negative),
                    'gets_per_negative': divide(playerScore.n_power + playerScore.n_tossup, playerScore.n_negative)
                }

                game_statistics['points_per_tossup'] = divide(game_statistics['total'], game_statistics['n_heard'])

                statistics['games'].append(game_statistics)

            for individual in individual_data:
                if individual['player_id'] == player.id:
                    statistics['totals'] = individual

            scores.append(statistics)

    return scores

def round_report(tournament, supertournament):
    report = []

    if not tournament:
        rounds = [bracket.rounds for bracket in supertournament.tournaments]
        rounds = [round for bracket in rounds for round in bracket] 
    else:
        rounds = tournament.rounds.order_by(Round.number).all()

    for round in rounds:
        round_data = {
            'number': round.number,
            'bracket': round.tournament.name,
            'total': 0,
            'games_played': round.games.count(),
            'player_points': 0,
            'bonus_points': 0,
            'bonuses_heard': 0
        }

        for game in round.games:
            for teamScore in game.teamScores:
                round_data['total'] += get_total_score(teamScore)

            for playerScore in game.playerScores:
                round_data['player_points'] += 15 * playerScore.n_power + 10 * playerScore.n_tossup - 5 * playerScore.n_negative
                round_data['bonuses_heard'] += playerScore.n_power + playerScore.n_tossup

        round_data['bonus_points'] = round_data['total'] - round_data['player_points']

        round_data['points_per_bonus'] = divide(round_data['bonus_points'], round_data['bonuses_heard'])
        round_data['points_per_game'] = divide(round_data['total'], round_data['games_played'])

        report.append(round_data)

    return report

def divide(a, b):
    try:
        value = a / float(b)
    except ZeroDivisionError:
        value = -1
    finally:
        return value

def get_total_score(teamScore):
    team = teamScore.team
    game = teamScore.game
    
    player_total = 0

    for playerScore in game.playerScores:
        if playerScore.player in team.players:
            player_total += 15 * playerScore.n_power + 10 * playerScore.n_tossup - 5 * playerScore.n_negative

    return teamScore.bonus_total + player_total

def get_points_per_bonus(teamScore):
    game = teamScore.game
    bonuses_heard = 0
    for player in teamScore.team.players:
        playerScore = game.playerScores.filter_by(player_id = player.id).first()
        bonuses_heard += playerScore.n_power + playerScore.n_tossup
    
    return divide(teamScore.bonus_total, bonuses_heard)

#Returns only the teamScores that are in the current bracket
def get_relevant_teamScores(team, tournament):
    return [teamScore for teamScore in team.teamScores if teamScore.game in get_bracket_games(tournament)]

#Returns only the playerScores that are in the current bracket
def get_relevant_playerScores(player, tournament):
    return [playerScore for playerScore in player.playerScores if playerScore.game in get_bracket_games(tournament)]

##############################
###### EDIT TOURNAMENT #######
##############################
@app.route('/get_edit_data/<int:supertournament_id>')
def get_edit_data(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)
    
    return jsonify({
        'supertournament':supertournament.name,
        'directors':get_directors(supertournament),
        'moderators':get_moderators(supertournament),
        'teams':get_teams(supertournament),
        'brackets':get_brackets(supertournament),
        'rounds':get_rounds(supertournament)
    })

def get_teams(supertournament):
    teams = []
    for team in supertournament.teams.order_by(Team.name):
        players = []

        for player in team.players:
            players.append({'id':player.id, 'name':player.name})

        divisions = dict((division.tournament.id, division.id) for division in team.divisions)

        teams.append({'id':team.id, 'name':team.name, 'divisions':divisions, 'players':players})

    return teams

def get_directors(supertournament):
    return [{'id':director.id, 'email':director.email} for director in supertournament.directors]

def get_moderators(supertournament):
    return [{'id':moderator.id, 'email':moderator.email} for moderator in supertournament.moderators]

def get_brackets(supertournament):
    brackets = {}

    for tournament in supertournament.tournaments:
        bracket = {
            'name':tournament.name,
            'n_round':tournament.rounds.count(),
            'rounds':get_bracket_rounds(tournament),
            'divisions':get_divisions(tournament)
        }

        brackets[tournament.id] = bracket
    
    return brackets

def get_bracket_rounds(tournament):
    rounds = {}
    for round in tournament.rounds:
        rounds[round.number] = []
        for game in round.games:
            teamScores = game.teamScores.all()
            rounds[round.number].append({
                'id':game.id,
                '0':{
                    'team_name':teamScores[0].team.name,
                    'total':get_total_score(teamScores[0])
                },
                '1':{
                    'team_name':teamScores[1].team.name,
                    'total':get_total_score(teamScores[1])
                }
            })
    
    return rounds

def get_rounds(supertournament): 
    all_rounds = {}
    for tournament in supertournament.tournaments:
        rounds = {}
        for round in tournament.rounds:
            rounds[round.number] = []
            for game in round.games:
                teamScores = game.teamScores.all()
                rounds[round.number].append({
                    'id':game.id,
                    '0':{
                        'team_name':teamScores[0].team.name,
                        'total':get_total_score(teamScores[0])
                    },
                    '1':{
                        'team_name':teamScores[1].team.name,
                        'total':get_total_score(teamScores[1])
                    }
                })

        all_rounds[tournament.id] = rounds

    return all_rounds

def get_divisions(tournament):
    return dict((division.id, division.name) for division in tournament.divisions)

##### TOURNAMENT SETTINGS #####
@app.route('/edit_settings/<int:supertournament_id>', methods = ['POST'])
def edit_settings(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)

    settings = request.json
    
    schema = Schema({
        Required('tournament_name'): All(Coerce(str), Length(min = 1))
    })

    try:
        schema(settings)
    except (Invalid, MultipleInvalid) as e:
        abort(400)

    supertournament.name = settings['tournament_name']
    db.session.commit()

    
         

    return jsonify({'message':'Successfully edited settings.'})

@app.route('/delete_tournament/<int:supertournament_id>')
def delete_tournament(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)
    supertournament.directors = []
    supertournament.moderators = []

    for team in supertournament.teams:
        team.divisions = []

    db.session.commit()

    db.session.delete(supertournament) #Deletes Rounds, Games, PlayerScores, TeamScores, Teams, Players via cascade

    db.session.commit()

    
         

    return 'Successfully deleted tournament.'

### DIRECTORS AND MODERATORS ###
@app.route('/add_credentials/<int:supertournament_id>', methods = ['POST'])
def add_credentials(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    user_emails = request.json['emails']
    type = request.json['type']

    supertournament = Supertournament.query.get(supertournament_id)

    users, non_users = verify_users_by_email(user_emails)

    if type == 'director':
        supertournament.directors += users
    else:
        supertournament.moderators += users

    db.session.commit()

    
         

    if non_users:
        response = jsonify({'code':400, 'message':'BAD_REQUEST', 'non_users':non_users})
        response.status_code = 400
        return response
    else:
        response = jsonify({'code':200, 'message':'All ' + type + 's added.', 'users':[user.email for user in users], 'non_users':[]})
        response.status_code = 200
        return response

def verify_users_by_email(user_emails):
    users = []
    non_users = []

    for user_email in user_emails:
        user = User.query.filter_by(email = user_email).first()
        if user:
            users.append(User.query.filter_by(email = user_email).first())
        else:
            non_users.append(user_email)

    return users, non_users

@app.route('/delete_credentials/<int:supertournament_id>/args')
def delete_credentials(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    type = request.args.get('type', '', type = str)
    user_id = request.args.get('user_id', 0, type = int)

    supertournament = Supertournament.query.get(supertournament_id)
    user = User.query.get(user_id)

    if type == 'director' and len(supertournament.directors) == 1:
        abort(400) 

    if type == 'director':
        user.directed_supertournaments.remove(supertournament)
    else:
        user.moderated_supertournaments.remove(supertournament)

    db.session.commit()

    
         

    return 'Deleted credential.'

########## ALL TEAMS ##########
@app.route('/add_team/<int:supertournament_id>', methods = ['POST'])
def add_team(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)

    new_team = request.json

    schema = Schema({
        Required('divisions'): {
            Coerce(int): int 
        },
        Required('name'): All(unicode, Length(min = 1)),
        Required('players'): [
            All(unicode, Length(min = 1))
        ]
    })

    try:
        schema(new_team)
    except MultipleInvalid as e:
        abort(400)

    team = Team(new_team['name'])

    for player_name in new_team['players']:
        player = Player(player_name)
        team.players.append(player)

    for bracket_id in new_team['divisions']:
        bracket = Tournament.query.get(bracket_id)
        
        division = Division.query.get(new_team['divisions'][bracket_id])
        
        if division not in bracket.divisions:
            abort(403)
        
        team.divisions.append(division)

    supertournament.teams.append(team)

    db.session.commit()    

    
         

    return jsonify({'message':'Successfully added team'})

@app.route('/import_teams/<int:supertournament_id>', methods = ['POST'])
def import_teams(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get_or_404(supertournament_id)

    teams = request.json['teams']
    
    malformed = False
    
    team_name = ''
    division_names = {}
    player_names = []

    mode = 'team'    
    for line in teams.splitlines():
        if line != '.' and line != '-':
            if mode == 'team':
                team_name = line
                mode = 'division'
            elif mode == 'division':
                division_id = line.split('>')
                division_names[division_id[0]] = division_id[1] #division_names[bracket_name] = division_name
            elif mode == 'player':
                player_names.append(line)
        elif line == '-':
            mode = 'player'
        elif line == '.':
            mode = 'team'
            team = Team(team_name)
            for player_name in player_names:
                player = Player(player_name)
                team.players.append(player)
            
            for bracket_name in division_names:
                bracket = Tournament.query.filter_by(name = bracket_name).first()
                
                if bracket:
                    bracket_divisions = Division.query.filter_by(tournament_id = bracket.id)

                    division_name = division_names[bracket_name]                    
                    division = bracket_divisions.filter_by(name = division_name).first()
                    alternate_division = bracket_divisions.first()

                    if division:
                        division.teams.append(team)
                    else:
                        malformed = 'Division ' + division_name + 'does not exist.'
                        alternate_division.teams.append(team)
                else: #Bracket does not exist, filling in divisions for ALL brackets with arbitrary first divisions
                    malformed = 'Bracket ' + bracket_name + ' does not exist'
                    
                    for bracket in supertournament.tournaments:
                        alternate_division = bracket.divisions.first()
                        
                        alternate_division.teams.append(team)

                    break

                db.session.commit()
                 
                if len(team.divisions) != supertournament.tournaments.count():
                    malformed = 'Not all divisions were assigned for team ' + team_name
                    for bracket in supertournament.tournaments:
                        teams = []
                        
                        for division in bracket.divisions:
                            teams += division.teams
                        
                        if team not in teams:
                            alternate_division = bracket.divisions.first()
                            alternate_division.teams.append(team) 
            
            supertournament.teams.append(team)
            db.session.commit()

            team_name = ''
            player_names = []
            division_names = {}

    db.session.commit()    
    
    
         
    
    if not malformed:
        return jsonify({'message':'Successfully imported teams'})
    else:
        return jsonify({'message':'MALFORMED DATA'})
    
@app.route('/edit_teams/<int:supertournament_id>', methods = ['POST'])
def edit_teams(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)

    new_teams = request.json
    
    schema = Schema({
        Required('team'): {
            Coerce(int): All(unicode, Length(min = 1))
        },
        Required('player'): {
            Coerce(int): unicode
        },
        Required('division'): {
            Coerce(int): {
                Coerce(int): All(int, Range(min = 1)) 
            }
        },
        Required('new_player'): {
            Coerce(int): [
                unicode
            ]
        }
    })

    try:
        schema(new_teams)
    except MultipleInvalid as e:
        abort(400)

    for team_id in new_teams['team']:
        team = Team.query.get(team_id)

        if not team:
            abort(404)
        if team not in supertournament.teams:
            abort(403)

        team.name = new_teams['team'][team_id]

        for bracket_id in new_teams['division'][team_id]:
            division = Division.query.get(new_teams['division'][team_id][bracket_id])
            tournament = Tournament.query.get(bracket_id)
            
            if not division or not tournament:
                abort(404)
            
            if division not in [flat_division for flat_tournament in supertournament.tournaments for flat_division in flat_tournament.divisions]:
                abort(403)

            if tournament not in supertournament.tournaments:
                abort(403)
            
            current_division = get_unique_division(tournament, team) 
            team.divisions.remove(current_division)
            team.divisions.append(division)

    for team_id in new_teams['new_player']:
        team = Team.query.get(team_id)

        if not team:
            abort(400)

        if team not in supertournament.teams:
            abort(403)

        for player_name in new_teams['new_player'][team_id]:
            if player_name:
                player = Player(player_name)
                team.players.append(player)
                
                db.session.commit()

                teamScores = TeamScore.query.filter_by(team_id = team.id)
                   
                if teamScores:
                    for teamScore in teamScores:
                        game = teamScore.game
                        game.playerScores.append(PlayerScore(0, 0, 0, 0, player_id = player.id))

    #Smarter logic would be to have a data structure where the players under each team, then can verify player is in those teams rather than all teams in the tournament
    for player_id in new_teams['player']:
        name = str(new_teams['player'][player_id]).strip()
        player = Player.query.get(player_id)

        players = [subplayer for subteam in supertournament.teams for subplayer in subteam.players]

        if not player:
            abort(400)

        if player not in players:
            abort(403)

        if name:
            player.name = name
        else:
            db.session.delete(player)

    db.session.commit()
    
    
         
    
    return jsonify({'message':'Successfully edited teams'})

@app.route('/delete_team/<int:supertournament_id>/<int:team_id>')
def delete_team(supertournament_id = 0, team_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)
    team = Team.query.get(team_id)

    if not team:
        abort(400)

    if team not in supertournament.teams:
        abort(403)

    teamScores = TeamScore.query.filter_by(team_id = team.id).all()

    if teamScores:
        for teamScore in teamScores:
            db.session.delete(teamScore.game) #Delete all Games that involve the Team, then every PlayerScore and TeamScore of both Teams via cascade
   
    team.divisions = []
    
    db.session.commit()
     
    db.session.delete(team) #Delete Team, then every Player via cascade

    db.session.commit()
    
    
         

    return 'Successfully deleted team'

@app.route('/add_bracket/<int:supertournament_id>', methods = ['POST'])
def add_bracket(supertournament_id = 0):
    credential_check(supertournament_id, 'director')
    
    supertournament = Supertournament.query.get(supertournament_id)
    
    new_bracket = request.json
    
    bracket = Tournament(new_bracket['name'])
    n_round = new_bracket['n_round']
    
    for i in range(0, n_round):
        bracket.rounds.append(Round(i + 1))
   
    division = Division('Main Division') 
    bracket.divisions.append(division)

    supertournament.tournaments.append(bracket)

    for team in supertournament.teams:
        team.divisions.append(division)
    
    db.session.commit()
    
    
         
    
    return jsonify({'message':'Successfully added bracket'})

########## BRACKETS AND GAMES ##########
def validate_divisions(divisions, add_divisions, tournament):
    is_empty = True
    not_in_tournament = False

    for division_id in divisions:
        if divisions[division_id].strip():
            is_empty = False
        division = Division.query.get(division_id)
        if not division:
            abort(400)

        if division not in tournament.divisions:
            abort(403)

    for add_division in add_divisions:
        if add_division.strip():
            is_empty = False

    if is_empty:
        raise Invalid('Your tournament must have at least one division.')

#Security unverified - make sure everything descends from supertournament.
#Doesn't validate properly for new brackets > divs data structure.
@app.route('/edit_brackets/<int:supertournament_id>/args', methods = ['POST'])
def edit_brackets(supertournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)

    brackets = request.json
    
    schema = Schema({
        Required('round'): {
            Coerce(int): All(int, Range(min = 1)),
        },
        Required('division'): {
            Coerce(int): {
                Coerce(int): All(Coerce(str)) 
            }
        },
        Required('bracket'): {
            Coerce(int): All(Coerce(str))
        },
        Required('add_division'): {
            Coerce(int): [
                All(Coerce(str))
            ]
        }
    })

    try:
        schema(brackets)
        #FIX VALIDATE DIVISIONS TO WORK FOR BRACKETS AND DIVISIONS. ONE LEVEL DEEPER ETC
        #validate_divisions(brackets['division'], settings['add_division'], tournament)
    except (Invalid, MultipleInvalid) as e:
        abort(400)

    for bracket_id in brackets['bracket']:
        tournament = Tournament.query.get(bracket_id)
        
        if not tournament:
            abort(404)
        
        tournament.name = brackets['bracket'][bracket_id]

    for bracket_id in brackets['add_division']:
        tournament = Tournament.query.get(bracket_id)

        if not tournament:
            abort(404)

        for division_name in brackets['add_division'][bracket_id]:
            tournament.divisions.append(Division(division_name))

    for bracket_id in brackets['division']:
        for division_id in brackets['division'][bracket_id]:
            division = Division.query.get(division_id)
            
            if not division:
                abort(404)
            
            if division not in [subdivision for subtournament in supertournament.tournaments for subdivision in subtournament.divisions]:
                abort(403)

            division_name = brackets['division'][bracket_id]
            if division_name:
                alternate_division = Division.query.get(division_id)
    #Purpose of above method is to find an alternate division that exists that can be used if a division is deleted, for the teams to be assigned to.

    for bracket_id in brackets['division']:
        for division_id in brackets['division'][bracket_id]:
            division_name = brackets['division'][bracket_id][division_id]
            
            division = Division.query.get(division_id)
           
            if division_name: 
                division.name = brackets['division'][bracket_id][division_id]
            else:
                division.teams = []
                db.session.commit()
                db.session.delete(division)
         
    for bracket_id in brackets['round']:
        tournament = Tournament.query.get(bracket_id)
        current = tournament.rounds.count()
        new = int(brackets['round'][bracket_id])
     
        if current < new:
            for i in range(current, new):
                tournament.rounds.append(Round(i + 1))
        elif current > new:
            for i in range(new, current):
                tournament.rounds.remove(tournament.rounds.filter_by(number = i + 1).first()) #Deletes all Games, TeamScores, and PlayerScores via cascade

    db.session.commit()
    
    
         

    return jsonify({'message':'Successfully edited settings.'})

@app.route('/delete_bracket/<int:supertournament_id>/<int:tournament_id>')
def delete_bracket(supertournament_id = 0, tournament_id = 0):
    credential_check(supertournament_id, 'director')

    supertournament = Supertournament.query.get(supertournament_id)
    bracket = Tournament.query.get(tournament_id)

    if not bracket:
        abort(404)

    if bracket not in supertournament.tournaments:
        abort(403)

    for division in bracket.divisions:
        division.teams = []

    db.session.commit()

    db.session.delete(bracket)
    db.session.commit()
    
    
         

    return jsonify({'message':'Successfully deleted bracket'})

@app.route('/add_game/<int:supertournament_id>/<int:tournament_id>', methods = ['POST'])
def add_game(supertournament_id = 0, tournament_id = 0):
    credential_check(supertournament_id, 'director')
    
    scores = request.json
    schema = Schema({
        Required('teams'): {
            Coerce(int): {
                'heard': All(int, Range(min = 0)),
                'total': All(int, Range(min = 0))
            }
        },
        Required('players'): {
            Coerce(int): {
                'heard': All(int, Range(min = 0)),
                'power': All(int, Range(min = 0)),
                'tossup': All(int, Range(min = 0)),
                'negative': All(int, Range(min = 0))
            }
        },
        Required('n_round'): All(int, Range(min = 1))
    })

    try:
        schema(scores)
        validate_add_game(scores)
    except (Invalid, MultipleInvalid) as e:
        abort(400)
   
    supertournament = Supertournament.query.get(supertournament_id)
    tournament = Tournament.query.get(tournament_id)

    if not tournament:
        abort(404)

    if tournament not in supertournament.tournaments:
        abort(403)

    round = Round.query.filter_by(tournament_id = tournament.id).filter_by(number = request.json['n_round']).first()

    if tournament not in supertournament.tournaments:
        abort(400)

    if round not in tournament.rounds:
        abort(400) #Make more robust, validate

    game = Game()
    round.games.append(game)
    db.session.commit()

    for player_id in scores['players']:
        player = Player.query.get(player_id)
	
        if player not in sum([Team.query.get(team_id).players.all() for team_id in scores['teams'].keys()], []):
            abort(400) #validate

        score = scores['players'][player_id]
        playerScore = PlayerScore(score['power'], score['tossup'], score['negative'], score['heard'], player_id = player.id)
        game.playerScores.append(playerScore)

    db.session.commit()

    for team_id in scores['teams']:
        team = Team.query.get(team_id)

        if team not in supertournament.teams:
            abort(400) # validate
    
        total_player_score = 0
        for player_id in scores['players']:
            player = Player.query.get(player_id)
            if player in team.players:
                score = scores['players'][player_id]
                total_player_score += 15 * score['power']
                total_player_score += 10 * score['tossup']
                total_player_score -= 5 * score['negative']
        bonus_total = scores['teams'][team_id]['total'] - total_player_score

        teamScore = TeamScore(bonus_total, scores['teams'][team_id]['heard'], team_id = team.id)
        game.teamScores.append(teamScore)
    
    db.session.commit()
    
    
         

    return jsonify({'message':'Added game.'})

def validate_add_game(scores):
    n_players = 0

    for team_id in scores['teams']:
        if team_id == -1:
            raise Invalid('At least one team is not selected.')
        if not Team.query.get(team_id):
            raise Invalid('At least one team does not exist.')
        n_players += Team.query.get(team_id).players.count()

    for player_id in scores['players']:
        if not Player.query.get(player_id):
            raise Invalid('At least one player does not exist.')
        
        player = scores['players'][player_id]

        if player['power'] + player['tossup'] + player['negative'] > player['heard']:
            raise Invalid('The player answered more questions than heard')

    if len(scores['players'].keys()) != n_players:
        raise Invalid('Some players do not have statistics entries.')

@app.route('/delete_game/<int:supertournament_id>/<int:tournament_id>/<int:game_id>')
def delete_game(supertournament_id = 0, tournament_id = 0, game_id = 0):
    credential_check(supertournament_id, 'director')

    game = Game.query.get(game_id)
    if not game:
        abort(400)
    
    supertournament = Supertournament.query.get(supertournament_id)
    tournament = Tournament.query.get(tournament_id)

    if tournament not in supertournament.tournaments:
        abort(403)
    
    if game not in get_bracket_games(tournament): 
        abort(403)

    db.session.delete(game)
    db.session.commit()
    
    
         

    return 'Deleted game.'

#############################
######### myABACUS ##########
#############################

@app.route('/auth/login', methods=['GET', 'POST'])
def login():
    resp = requests.post(app.config['PERSONA_VERIFIER'], data = {
        'assertion':request.form['assertion'],
        'audience':request.host_url
    }, verify = True)

    if resp.ok:
        verification_data = json.loads(resp.content)
        if verification_data['status'] == 'okay':
            session['email'] = verification_data['email']

            new_email = session['email']

            user = User.query.filter_by(email = new_email).first()
            if user is None:
                db.session.add(User(new_email))
                db.session.commit()

            return 'Logged in.'

    abort(400)

@app.route('/auth/logout', methods=['POST'])
def logout():
    session.clear()
    return 'Logged out.'

#############################
######## UTILITIES ##########
#############################

@app.route('/supertournaments_directed') 
def supertournaments_directed():
    user = User.query.filter_by(email = g.user).first()
    return tournaments_credentialed(user, ['director']) 

@app.route('/supertournaments_moderated')
def supertournaments_moderated():
    user = User.query.filter_by(email = g.user).first()
    return tournaments_credentialed(user, ['moderator'])

@app.route('/supertournaments_directed_moderated')
def supertournaments_directed_moderated():
    user = User.query.filter_by(email = g.user).first()
    return tournaments_credentialed(user, ['director', 'moderator'])

#Returns the division of the team that is in a specific bracket
def get_unique_division(tournament, team):
    return list(set(tournament.divisions.all()) & set(team.divisions))[0]

#Returns a flat list of all games in a bracket
def get_bracket_games(tournament):
    bracket_games = [round.games for round in tournament.rounds]
    flat_bracket_games = [subgame for subround in bracket_games for subgame in subround]
    return flat_bracket_games

def tournaments_credentialed(user, credentials): #Credentials is a list containing either or both 'director', 'moderator'
    supertournaments = []

    raw_supertournaments = []
    
    if 'director' in credentials:
        raw_supertournaments += user.directed_supertournaments     
    
    if 'moderator' in credentials:
        raw_supertournaments += user.moderated_supertournaments     

    for supertournament in raw_supertournaments: 
        supertournaments.append({
            'id':supertournament.id, 
            'name':supertournament.name
        })

    return jsonify({'supertournaments':supertournaments})
    
def has_credential(supertournament_id, user_email, mode):
    supertournament = Supertournament.query.get(supertournament_id)
    if not supertournament:
        return False

    user = User.query.filter_by(email = user_email).first()

    if mode == 'director':
        if not user or user not in supertournament.directors:
            return False
    else:
        if not user or user not in supertournament.moderators:
            return False

    return True

def credential_check(supertournament_id, mode): #Checks only for current user, and checks for level - a director has credentials of a moderator
    Supertournament.query.get_or_404(supertournament_id)

    if not g.user:
        abort(401)

    if mode == 'director':
        if not has_credential(supertournament_id, g.user, mode):
            abort(403)
    else: #Moderator
        if not has_credential(supertournament_id, g.user, mode) and not has_credential(supertournament_id, g.user, 'director'):
            abort(403)

def generate_statistic_report_titles(supertournament):
    return [{'id':tournament.id, 'name':tournament.name} for tournament in supertournament.tournaments]

@app.route('/tournament_games/<int:supertournament_id>')
def tournament_games(supertournament_id = 0):
    supertournament = Supertournament.query.get_or_404(supertournament_id)
    games = []
    for bracket in supertournament.tournaments:
        for round in bracket.rounds:
            for game in round.games:
                if game.teamScores.all():
                    one = game.teamScores.all()[0]
                    two = game.teamScores.all()[1]
                    games.append({
                        'Bracket':bracket.name,
                        'Round':round.number,
                        'Team One':one.team.name,
                        'Team One Score':get_total_score(one),
                        'Team Two':two.team.name,
                        'Team Two Score':get_total_score(two)
                    })

    return jsonify({'games':games})

#############################
####### MISCELLANEOUS #######
#############################
def debug(text):
    log = open(app.config['LOG_FILE'], 'a')
    log.write(str(text) + '\n')
    log.write('------------------\n')
    log.close()

def flush_debug():
    log = open(app.config['LOG_FILE'], 'w')
    log.close()

if __name__ == '__main__':
    app.run(host = '0.0.0.0', port = int('5000'), debug = True)
